﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace LogicApp
{
    public abstract class SqlHelper
    {
        private static Hashtable parmCache = Hashtable.Synchronized(new Hashtable());

        public static string CONN_STRING
        {
            get
            {
                return "Data Source=localhost;Initial Catalog=GPS;User Id=justdoit;Password=iloveyou;";
            }
        }

        public static string TransSql(params string[] sql)
        {
            using (SqlConnection sqlConnection = new SqlConnection(SqlHelper.CONN_STRING))
            {
                sqlConnection.Open();
                using (SqlTransaction trans = sqlConnection.BeginTransaction())
                {
                    try
                    {
                        for (int index = 0; index < sql.Length; ++index)
                            SqlHelper.ExecuteNonQuery(trans, sql[index]);
                        trans.Commit();
                        return "";
                    }
                    catch (Exception ex)
                    {
                        trans.Rollback();
                        return ex.Message + " ";
                    }
                }
            }
        }

        public static DataSet ExecuteDataset(string commandText)
        {
            return SqlHelper.ExecuteDataset(commandText, (SqlParameter[])null);
        }

        public static DataSet ExecuteDataset(SqlTransaction trans, string commandText)
        {
            return SqlHelper.ExecuteDataset(trans, CommandType.Text, commandText, (SqlParameter[])null);
        }

        public static DataSet ExecuteDataset(
          SqlTransaction trans,
          string commandText,
          params SqlParameter[] commandParameters)
        {
            return SqlHelper.ExecuteDataset(trans, CommandType.Text, commandText, commandParameters);
        }

        public static DataSet ExecuteDataset(
          string commandText,
          params SqlParameter[] commandParameters)
        {
            return SqlHelper.ExecuteDataset(CommandType.Text, commandText, commandParameters);
        }

        public static DataSet ExecuteDataset(
          CommandType commandType,
          string commandText,
          params SqlParameter[] commandParameters)
        {
            return SqlHelper.ExecuteDataset((SqlTransaction)null, commandType, commandText, commandParameters);
        }

        public static DataSet ExecuteDataset(
          SqlTransaction trans,
          CommandType commandType,
          string commandText,
          params SqlParameter[] commandParameters)
        {
            if (trans == null)
            {
                using (SqlConnection conn = new SqlConnection(SqlHelper.CONN_STRING))
                {
                    conn.Open();
                    SqlCommand sqlCommand = new SqlCommand();
                    SqlHelper.PrepareCommand(sqlCommand, conn, (SqlTransaction)null, commandType, commandText, commandParameters);
                    using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand))
                    {
                        DataSet dataSet = new DataSet();
                        sqlDataAdapter.Fill(dataSet);
                        sqlCommand.Parameters.Clear();
                        return dataSet;
                    }
                }
            }
            else
            {
                using (SqlCommand sqlCommand = new SqlCommand(commandText, trans.Connection, trans))
                {
                    SqlHelper.PrepareCommand(sqlCommand, sqlCommand.Connection, (SqlTransaction)null, commandType, commandText, commandParameters);
                    using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand))
                    {
                        DataSet dataSet = new DataSet();
                        sqlDataAdapter.Fill(dataSet);
                        sqlCommand.Parameters.Clear();
                        return dataSet;
                    }
                }
            }
        }

        public static int ExecuteNonQuery(string cmdText)
        {
            return SqlHelper.ExecuteNonQuery(CommandType.Text, cmdText, (SqlParameter[])null);
        }

        public static int ExecuteNonQuery(string cmdText, params SqlParameter[] cmdParms)
        {
            return SqlHelper.ExecuteNonQuery(CommandType.Text, cmdText, cmdParms);
        }

        public static int ExecuteNonQuery(
          CommandType cmdType,
          string cmdText,
          params SqlParameter[] cmdParms)
        {
            SqlCommand cmd = new SqlCommand();
            using (SqlConnection conn = new SqlConnection(SqlHelper.CONN_STRING))
            {
                cmd.CommandTimeout = 300;
                SqlHelper.PrepareCommand(cmd, conn, (SqlTransaction)null, cmdType, cmdText, cmdParms);
                int num = cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
                return num;
            }
        }

        public static int ExecuteNonQuery(
          SqlConnection conn,
          CommandType cmdType,
          string cmdText,
          params SqlParameter[] cmdParms)
        {
            SqlCommand cmd = new SqlCommand();
            SqlHelper.PrepareCommand(cmd, conn, (SqlTransaction)null, cmdType, cmdText, cmdParms);
            int num = cmd.ExecuteNonQuery();
            cmd.Parameters.Clear();
            return num;
        }

        public static int ExecuteNonQuery(SqlTransaction trans, string cmdText)
        {
            return SqlHelper.ExecuteNonQuery(trans, CommandType.Text, cmdText, (SqlParameter[])null);
        }

        public static int ExecuteNonQuery(
          SqlTransaction trans,
          string cmdText,
          params SqlParameter[] cmdParms)
        {
            return SqlHelper.ExecuteNonQuery(trans, CommandType.Text, cmdText, cmdParms);
        }

        public static int ExecuteNonQuery(
          SqlTransaction trans,
          CommandType cmdType,
          string cmdText,
          params SqlParameter[] cmdParms)
        {
            SqlCommand cmd = new SqlCommand();
            SqlHelper.PrepareCommand(cmd, trans.Connection, trans, cmdType, cmdText, cmdParms);
            int num = cmd.ExecuteNonQuery();
            cmd.Parameters.Clear();
            return num;
        }

        public static SqlDataReader ExecuteReader(string cmdText)
        {
            return SqlHelper.ExecuteReader(CommandType.Text, cmdText, (SqlParameter[])null);
        }

        public static SqlDataReader ExecuteReader(SqlTransaction trans, string cmdText)
        {
            return SqlHelper.ExecuteReader(trans, CommandType.Text, cmdText, (SqlParameter[])null);
        }

        public static SqlDataReader ExecuteReader(
          string cmdText,
          params SqlParameter[] cmdParms)
        {
            return SqlHelper.ExecuteReader(CommandType.Text, cmdText, cmdParms);
        }

        public static SqlDataReader ExecuteReader(
          SqlTransaction trans,
          string cmdText,
          params SqlParameter[] cmdParms)
        {
            return SqlHelper.ExecuteReader(trans, CommandType.Text, cmdText, cmdParms);
        }

        public static SqlDataReader ExecuteReader(
          CommandType cmdType,
          string cmdText,
          params SqlParameter[] cmdParms)
        {
            return SqlHelper.ExecuteReader((SqlTransaction)null, cmdType, cmdText, cmdParms);
        }

        public static SqlDataReader ExecuteReader(
          SqlTransaction trans,
          CommandType cmdType,
          string cmdText,
          params SqlParameter[] cmdParms)
        {
            if (trans == null)
            {
                SqlCommand cmd = new SqlCommand();
                SqlConnection conn = new SqlConnection(SqlHelper.CONN_STRING);
                try
                {
                    SqlHelper.PrepareCommand(cmd, conn, (SqlTransaction)null, cmdType, cmdText, cmdParms);
                    SqlDataReader sqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    cmd.Parameters.Clear();
                    return sqlDataReader;
                }
                catch
                {
                    conn.Close();
                    throw;
                }
            }
            else
            {
                using (SqlCommand cmd = new SqlCommand(cmdText, trans.Connection, trans))
                {
                    SqlHelper.PrepareCommand(cmd, cmd.Connection, trans, cmdType, cmdText, cmdParms);
                    return cmd.ExecuteReader();
                }
            }
        }

        public static bool Exists(SqlTransaction trans, string cmdText)
        {
            using (SqlCommand sqlCommand = new SqlCommand(cmdText, trans.Connection, trans))
                return sqlCommand.ExecuteReader().HasRows;
        }

        public static bool Exists(string cmdText)
        {
            using (SqlDataReader sqlDataReader = SqlHelper.ExecuteReader(cmdText))
                return sqlDataReader.HasRows;
        }

        public static bool Exists(string cmdText, params SqlParameter[] cmdParms)
        {
            using (SqlDataReader sqlDataReader = SqlHelper.ExecuteReader(cmdText, cmdParms))
                return sqlDataReader.HasRows;
        }

        public static bool Exists(SqlTransaction trans, string cmdText, params SqlParameter[] cmdParms)
        {
            using (SqlDataReader sqlDataReader = SqlHelper.ExecuteReader(trans, cmdText, cmdParms))
                return sqlDataReader.HasRows;
        }

        public static object ExecuteScalar(string cmdText)
        {
            return SqlHelper.ExecuteScalar(CommandType.Text, cmdText, (SqlParameter[])null);
        }

        public static object ExecuteScalar(string cmdText, params SqlParameter[] cmdParms)
        {
            return SqlHelper.ExecuteScalar(CommandType.Text, cmdText, cmdParms);
        }

        public static object ExecuteScalar(
          CommandType cmdType,
          string cmdText,
          params SqlParameter[] cmdParms)
        {
            SqlCommand cmd = new SqlCommand();
            using (SqlConnection conn = new SqlConnection(SqlHelper.CONN_STRING))
            {
                SqlHelper.PrepareCommand(cmd, conn, (SqlTransaction)null, cmdType, cmdText, cmdParms);
                object obj = cmd.ExecuteScalar();
                cmd.Parameters.Clear();
                return obj;
            }
        }

        public static object ExecuteScalar(SqlTransaction trans, string cmdText)
        {
            if (trans == null)
                return SqlHelper.ExecuteScalar(cmdText, (SqlParameter[])null);
            return SqlHelper.ExecuteScalar(trans, cmdText, (SqlParameter[])null);
        }

        public static object ExecuteScalar(
          SqlTransaction trans,
          string cmdText,
          params SqlParameter[] cmdParms)
        {
            return SqlHelper.ExecuteScalar(trans, CommandType.Text, cmdText, cmdParms);
        }

        public static object ExecuteScalar(
          SqlTransaction trans,
          CommandType cmdType,
          string cmdText,
          params SqlParameter[] cmdParms)
        {
            SqlCommand cmd = new SqlCommand();
            SqlHelper.PrepareCommand(cmd, trans.Connection, trans, cmdType, cmdText, cmdParms);
            object obj = cmd.ExecuteScalar();
            cmd.Parameters.Clear();
            return obj;
        }

        public static object ExecuteScalar(
          SqlConnection conn,
          CommandType cmdType,
          string cmdText,
          params SqlParameter[] cmdParms)
        {
            SqlCommand cmd = new SqlCommand();
            SqlHelper.PrepareCommand(cmd, conn, (SqlTransaction)null, cmdType, cmdText, cmdParms);
            object obj = cmd.ExecuteScalar();
            cmd.Parameters.Clear();
            return obj;
        }

        public static void CacheParameters(string cacheKey, params SqlParameter[] cmdParms)
        {
            SqlHelper.parmCache[(object)cacheKey] = (object)cmdParms;
        }

        public static SqlParameter[] GetCachedParameters(string cacheKey)
        {
            SqlParameter[] sqlParameterArray1 = (SqlParameter[])SqlHelper.parmCache[(object)cacheKey];
            if (sqlParameterArray1 == null)
                return (SqlParameter[])null;
            SqlParameter[] sqlParameterArray2 = new SqlParameter[sqlParameterArray1.Length];
            int index = 0;
            for (int length = sqlParameterArray1.Length; index < length; ++index)
                sqlParameterArray2[index] = (SqlParameter)((ICloneable)sqlParameterArray1[index]).Clone();
            return sqlParameterArray2;
        }

        private static void PrepareCommand(
          SqlCommand cmd,
          SqlConnection conn,
          SqlTransaction trans,
          CommandType cmdType,
          string cmdText,
          SqlParameter[] cmdParms)
        {
            if (conn.State != ConnectionState.Open)
                conn.Open();
            cmd.Connection = conn;
            cmd.CommandText = cmdText;
            if (trans != null)
                cmd.Transaction = trans;
            cmd.CommandType = cmdType;
            if (cmdParms == null)
                return;
            foreach (SqlParameter cmdParm in cmdParms)
                cmd.Parameters.Add(cmdParm);
        }
    }
}
