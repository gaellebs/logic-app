﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;

namespace LogicApp
{
    internal class Utils
    {
        public static Hashtable tracklistht = new Hashtable();
        public static Hashtable companyspeedht = new Hashtable();
        public static int smsconfig = 1;
        private static double EARTH_RADIUS = 6378.137;
        private const string url = "http://52.43.98.208/motabe/locationapi/updateGPSData";

        public static string ReserveGeocode(string latlng)
        {
            string str1 = "";
            try
            {
                string str2 = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)";
                HttpWebRequest httpWebRequest = WebRequest.Create("https://maps.googleapis.com/maps/api/geocode/json?latlng=" + HttpUtility.UrlEncode(latlng) + "&sensor=false&key=AIzaSyAvPM1wF9SewlSqZOLBQmYCl2rmZe-UOlc") as HttpWebRequest;
                httpWebRequest.Method = "GET";
                httpWebRequest.UserAgent = str2;
                str1 = new StreamReader((httpWebRequest.GetResponse() as HttpWebResponse).GetResponseStream(), Encoding.GetEncoding("UTF-8")).ReadToEnd();
                str1 = ((JArray)((JObject)JsonConvert.DeserializeObject(str1))["results"])[0][(object)"formatted_address"].ToString();
            }
            catch (Exception ex)
            {
            }
            return str1;
        }

        public static string OpenMapReserveGeocode(string latitude, string longitude)
        {
            string str1 = "";
            try
            {
                string requestUriString = "http://nominatim.openstreetmap.org/reverse?format=json&lat=" + latitude + "&lon=" + longitude + "&zoom=18&addressdetails=1";
                string str2 = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)";
                HttpWebRequest httpWebRequest = WebRequest.Create(requestUriString) as HttpWebRequest;
                httpWebRequest.Method = "GET";
                httpWebRequest.UserAgent = str2;
                str1 = new StreamReader((httpWebRequest.GetResponse() as HttpWebResponse).GetResponseStream(), Encoding.GetEncoding("UTF-8")).ReadToEnd();
                str1 = ((JObject)JsonConvert.DeserializeObject(str1))["display_name"].ToString();
                str1 = ((JObject)JsonConvert.DeserializeObject(str1))["display_name"].ToString();
            }
            catch (Exception ex)
            {
            }
            return str1;
        }

        public static int FC(double x1, double x2)
        {
            return x1 - x2 < 2E-06 && x1 - x2 > -2E-06 ? 1 : 0;
        }

        public static bool isPointOnLine(
          double p1x,
          double p1y,
          double p2x,
          double p2y,
          double px,
          double py)
        {
            double num1 = px - p1x;
            double num2 = p2x - p1x;
            double num3 = py - p1y;
            double num4 = p2y - p1y;
            return Utils.FC(num1 * num4 - num2 * num3, 0.0) != 0 && (Math.Min(p1x, p2x) <= px && px <= Math.Max(p1x, p2x) && (Math.Min(p1y, p2y) <= py && py <= Math.Max(p1y, p2y)));
        }

        private static double rad(double d)
        {
            return d * Math.PI / 180.0;
        }

        public static double GetDistance(double lat1, double lng1, double lat2, double lng2)
        {
            double num1 = Utils.rad(lat2 - lat1);
            double num2 = Utils.rad(lng2 - lng1);
            double d = Math.Sin(num1 / 2.0) * Math.Sin(num1 / 2.0) + Math.Cos(Utils.rad(lat1)) * Math.Cos(Utils.rad(lat2)) * Math.Sin(num2 / 2.0) * Math.Sin(num2 / 2.0);
            double num3 = 2.0 * Math.Atan2(Math.Sqrt(d), Math.Sqrt(1.0 - d));
            return Utils.EARTH_RADIUS * num3;
        }

        public static void SqlBulkCopyByDatatable(
          string connectionString,
          string TableName,
          DataTable dt)
        {
            using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(connectionString, SqlBulkCopyOptions.FireTriggers))
            {
                try
                {
                    sqlBulkCopy.BulkCopyTimeout = 300;
                    sqlBulkCopy.DestinationTableName = TableName;
                    for (int index = 0; index < dt.Columns.Count; ++index)
                        sqlBulkCopy.ColumnMappings.Add(dt.Columns[index].ColumnName, dt.Columns[index].ColumnName);
                    sqlBulkCopy.WriteToServer(dt);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public static void SendEmail(
          string host,
          string port,
          string username,
          string password,
          string address,
          string title,
          string content,
          string email)
        {
            new SmtpClient()
            {
                Host = host,
                Port = int.Parse(port),
                // UseDefaultCredentials = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                Credentials = ((ICredentialsByHost)new NetworkCredential(username, password)),
                EnableSsl = true
            }.Send(new MailMessage()
            {
                From = new MailAddress(address),
                To = {
                  email
                },
                Subject = title,
                Body = content,
                SubjectEncoding = Encoding.UTF8,
                BodyEncoding = Encoding.UTF8,
                Priority = MailPriority.High,
                IsBodyHtml = true
            });
        }

        public static void SendAttachmentEmail(
          string host,
          string port,
          string username,
          string password,
          string address,
          string title,
          string content,
          string email,
          string file)
        {
            SmtpClient smtpClient = new SmtpClient();
            smtpClient.Host = host;
            smtpClient.Port = int.Parse(port);
            smtpClient.UseDefaultCredentials = true;
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtpClient.Credentials = (ICredentialsByHost)new NetworkCredential(username, password);
            MailMessage message = new MailMessage();
            message.From = new MailAddress(address);
            message.To.Add(email);
            message.Subject = title;
            message.Body = content;
            message.SubjectEncoding = Encoding.UTF8;
            message.BodyEncoding = Encoding.UTF8;
            message.Priority = MailPriority.High;
            message.IsBodyHtml = true;
            Attachment attachment = new Attachment(file, "application/octet-stream");
            message.Attachments.Add(attachment);
            smtpClient.Send(message);
        }

        public static string SendSms(string content, string phoneno)
        {
            string str1 = "";
            Exception exception;
            switch (Utils.smsconfig)
            {
                case 1:
                    try
                    {
                        string requestUriString = "http://api.clickatell.com/http/sendmsg?user=poetmania&password=@see-curity2017Newpassword&api_id=3471075&to=" + phoneno + "&text=" + content;
                        string str2 = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)";
                        HttpWebRequest httpWebRequest = WebRequest.Create(requestUriString) as HttpWebRequest;
                        httpWebRequest.Method = "GET";
                        httpWebRequest.UserAgent = str2;
                        str1 = new StreamReader((httpWebRequest.GetResponse() as HttpWebResponse).GetResponseStream(), Encoding.GetEncoding("UTF-8")).ReadToEnd();
                        break;
                    }
                    catch (Exception ex)
                    {
                        exception = ex;
                        break;
                    }
                case 2:
                    try
                    {
                        str1 = Utils.HttpGet("http://www.smslive247.com/http/index.aspx?cmd=login&owneremail=tech-support@spytrac.com.ng&subacct=spytractelematics&subacctpwd=spytrac123");
                        if (str1.StartsWith("OK"))
                            str1 = Utils.HttpGet("http://www.smslive247.com/http/index.aspx?cmd=sendmsg&sessionid=" + str1.Split(':')[1].Trim() + "&message=" + content + "&sender=GPS&sendto=" + phoneno + "&msgtype=0");
                    }
                    catch (Exception ex)
                    {
                        exception = ex;
                    }
                    break;
            }
            return str1;
        }

        public static bool isPointInPolygon(string[] ps, double ALon, double ALat)
        {
            if (ps.Length < 3)
                return false;
            int num1 = 0;
            int length = ps.Length;
            for (int index = 0; index < length; ++index)
            {
                double num2;
                double num3;
                double num4;
                double num5;
                if (index == length - 1)
                {
                    num2 = double.Parse(ps[index].Split(',')[1]);
                    num3 = double.Parse(ps[index].Split(',')[0]);
                    num4 = double.Parse(ps[0].Split(',')[1]);
                    num5 = double.Parse(ps[0].Split(',')[0]);
                }
                else
                {
                    num2 = double.Parse(ps[index].Split(',')[1]);
                    num3 = double.Parse(ps[index].Split(',')[0]);
                    num4 = double.Parse(ps[index + 1].Split(',')[1]);
                    num5 = double.Parse(ps[index + 1].Split(',')[0]);
                }
                if ((ALat >= num3 && ALat < num5 || ALat >= num5 && ALat < num3) && Math.Abs(num3 - num5) > 0.0 && num2 - (num2 - num4) * (num3 - ALat) / (num3 - num5) < ALon)
                    ++num1;
            }
            return num1 % 2 != 0;
        }

        public static int GetWeekDay(DateTime d)
        {
            int num = 0;
            switch (DateTime.Today.DayOfWeek.ToString())
            {
                case "Monday":
                    num = 1;
                    break;
                case "Tuesday":
                    num = 2;
                    break;
                case "Wednesday":
                    num = 3;
                    break;
                case "Thursday":
                    num = 4;
                    break;
                case "Friday":
                    num = 5;
                    break;
                case "Saturday":
                    num = 6;
                    break;
                case "Sunday":
                    num = 7;
                    break;
            }
            return num;
        }

        public static string FormatDuration(double seconds)
        {
            return ((int)seconds / 3600).ToString() + ":" + ((int)(seconds % 3600.0) / 60).ToString() + ":" + ((int)seconds % 60).ToString();
        }

        public static string HttpGet(string url)
        {
            string str1 = "";
            try
            {
                string str2 = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)";
                HttpWebRequest httpWebRequest = WebRequest.Create(url) as HttpWebRequest;
                httpWebRequest.Method = "GET";
                httpWebRequest.UserAgent = str2;
                Stream responseStream = (httpWebRequest.GetResponse() as HttpWebResponse).GetResponseStream();
                StreamReader streamReader = new StreamReader(responseStream, Encoding.GetEncoding("UTF-8"));
                str1 = streamReader.ReadToEnd();
                streamReader.Close();
                responseStream.Close();
            }
            catch (Exception ex)
            {
            }
            return str1;
        }

        private static string buildParameters(
          string BusNo,
          string latitude,
          string longitude,
          string altitude,
          string speed,
          string date_time,
          string c_id)
        {
            return "access_token=E639A2AF-A2C0-8DEA-C63F-EB81E2578B11&salt=2136204503&sig=0PEG4BoeYY9a67SI3TY1UQ7YQe%2Bb8glEjGfZA26WGh8=&data=" + ("{\"bus_no\":\"" + BusNo + "\",\"latitude\":\"" + latitude + "\",\"longitude\":\"" + longitude + "\",\"altitude\":\"" + altitude + "\",\"speed\":\"" + speed + "\",\"date_time\":\"" + date_time + "\",\"c_id\":" + c_id + "}");
        }

        public static string Send(
          string BusNo,
          string latitude,
          string longitude,
          string altitude,
          string speed,
          string date_time,
          string c_id)
        {
            string s = Utils.buildParameters(BusNo, latitude, longitude, altitude, speed, date_time, c_id);
            string str = "";
            try
            {
                using (WebClient webClient = new WebClient())
                {
                    webClient.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                    str = Encoding.ASCII.GetString(webClient.UploadData("http://52.43.98.208/motabe/locationapi/updateGPSData", "POST", Encoding.ASCII.GetBytes(s)));
                }
            }
            catch (Exception ex)
            {
                str = ex.Message;
            }
            return str;
        }
    }
}
