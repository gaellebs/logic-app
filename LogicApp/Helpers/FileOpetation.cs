﻿using System;
using System.IO;


namespace LogicApp
{
    public class FileOpetation
    {
        public static void SaveRecord(string content)
        {
            if (string.IsNullOrEmpty(content))
                return;
            try
            {
                FileStream fileStream;
                using (fileStream = new FileStream(Path.Combine(AppDomain.CurrentDomain.SetupInformation.ApplicationBase+"/logs", string.Format("{0:yyyyMMdd}", (object)DateTime.Now)), FileMode.Append, FileAccess.Write))
                {
                    StreamWriter streamWriter;
                    using (streamWriter = new StreamWriter((Stream)fileStream))
                    {
                        streamWriter.Write(content);
                        streamWriter?.Close();
                    }
                    fileStream?.Close();
                }
            }
            catch
            {
            }
        }
    }
}
