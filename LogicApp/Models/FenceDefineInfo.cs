﻿namespace LogicApp
{
    internal class FenceDefineInfo
    {
        public int carfenceid;
        public int geofenceid;
        public int inorout;
        public string starttime;
        public string endtime;
        public int always;
        public string weekdates;
        public string fencedate;
        public string fencename;
        public int geofencekindid;
        public double radius;
        public string latlngs;
        public int cutoil;
    }
}
