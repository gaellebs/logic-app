﻿namespace LogicApp
{
    internal class CarInfo
    {
        public int carid;
        public string imeino;
        public string plate;
        public int protocol;
        public double timezone;
        public int companyid;
        public int state;
        public string mapinfo;
        public double basemileage;
        public string engineno;
    }
}
