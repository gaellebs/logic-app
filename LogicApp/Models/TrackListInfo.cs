﻿using System;

namespace LogicApp
{
    internal class TrackListInfo
    {
        public int carid;
        public string latitude;
        public string longitude;
        public DateTime gpstime;
        public int engine;
    }
}
