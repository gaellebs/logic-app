﻿using System;
namespace LogicApp
{
    internal class TrackListFenceInfo
    {
        public string imeino;
        public DateTime squaddate;
        public int carfenceid;
        public int geofenceid;
        public string startlatitude;
        public string startlongitude;
        public double startmileage;
        public DateTime starttime;
        public string endlatitude;
        public string endlongitude;
        public double endmileage;
        public DateTime endtime;
        public string duration;
        public string mileage;
    }
}
