﻿namespace LogicApp
{
    internal class RoundRouteInfo
    {
        public int roundrouteid;
        public int poiid;
        public double latitude;
        public double longitude;
        public double radius;
    }
}
