﻿namespace LogicApp
{
    internal class FenceCutOilDefineInfo
    {
        public int geofenceid;
        public int inorout;
        public string starttime;
        public string endtime;
        public int datetype;
        public string weekdates;
        public string areadate;
        public string fencename;
        public int geofencekindid;
        public double radius;
        public string latlngs;
    }
}
