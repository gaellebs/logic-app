﻿namespace LogicApp
{
    internal class POIInfo
    {
        public int id;
        public string name;
        public int companyid;
        public int poitypeid;
        public double latitude;
        public double longitude;
        public double radius;
    }
}
