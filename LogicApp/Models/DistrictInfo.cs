﻿namespace LogicApp
{
    internal class DistrictInfo
    {
        public int id;
        public int terminalid;
        public string name;
        public string terminalname;
        public string latlngs;
    }
}
