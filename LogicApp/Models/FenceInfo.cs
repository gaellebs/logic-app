﻿namespace LogicApp
{
    internal class FenceInfo
    {
        public int carfenceid;
        public int geofenceid;
        public string name;
        public int geofencekindid;
        public double radius;
        public string latlngs;
        public int inorout;
        public string starttime;
        public string endtime;
    }
}
