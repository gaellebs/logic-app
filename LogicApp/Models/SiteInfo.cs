﻿namespace LogicApp
{
    internal class SiteInfo
    {
        public int routeid;
        public int siteid;
        public int poiid;
        public double latitude;
        public double longitude;
        public double radius;
    }
}
