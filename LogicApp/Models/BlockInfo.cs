﻿namespace LogicApp
{
    internal class BlockInfo
    {
        public int id;
        public string name;
        public string latitude1;
        public string longitude1;
        public string latitude2;
        public string longitude2;
        public string latitude3;
        public string longitude3;
        public string latitude4;
        public string longitude4;
    }
}
