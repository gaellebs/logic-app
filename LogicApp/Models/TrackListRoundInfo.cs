﻿using System;


namespace LogicApp
{
    internal class TrackListRoundInfo
    {
        public string imeino;
        public DateTime squaddate;
        public int roundrouteid;
        public int poiid;
        public string exitlatitude;
        public string exitlongitude;
        public double exitmileage;
        public DateTime exittime;
        public string enterlatitude;
        public string enterlongitude;
        public double entermileage;
        public DateTime entertime;
    }
}
