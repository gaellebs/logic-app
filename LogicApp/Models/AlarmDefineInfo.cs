﻿namespace LogicApp
{
    internal class AlarmDefineInfo
    {
        public string sinput1on = "";
        public string sinput2on = "";
        public string sinput3on = "";
        public string sinput4on = "";
        public string sinput5on = "";
        public string sinput1off = "";
        public string sinput2off = "";
        public string sinput3off = "";
        public string sinput4off = "";
        public string sinput5off = "";
        public int input1on;
        public int input2on;
        public int input3on;
        public int input4on;
        public int input5on;
        public int input1off;
        public int input2off;
        public int input3off;
        public int input4off;
        public int input5off;
        public double speed;
        public int engine;
        public int working;
        public double moveminute;
        public double movespeed;
        public double moveinterval;
        public double togetherminute;
        public int engineon;
    }
}
