﻿namespace LogicApp
{
    internal class StatusDefineInfo
    {
        public string output1on = "";
        public string output2on = "";
        public string output3on = "";
        public string output4on = "";
        public string output5on = "";
        public string output1off = "";
        public string output2off = "";
        public string output3off = "";
        public string output4off = "";
        public string output5off = "";
        public string input1on = "";
        public string input2on = "";
        public string input3on = "";
        public string input4on = "";
        public string input5on = "";
        public string input1off = "";
        public string input2off = "";
        public string input3off = "";
        public string input4off = "";
        public string input5off = "";
    }
}
