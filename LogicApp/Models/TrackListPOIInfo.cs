﻿using System;

namespace LogicApp
{
    internal class TrackListPOIInfo
    {
        public string kind = "Unplan";
        public string imeino;
        public DateTime squaddate;
        public int roundrouteid;
        public int routeid;
        public int siteid;
        public int poiid;
        public string enterlatitude;
        public string enterlongitude;
        public double entermileage;
        public DateTime entertime;
        public string exitlatitude;
        public string exitlongitude;
        public double exitmileage;
        public DateTime exittime;
    }
}
