﻿namespace LogicApp
{
    internal class POIDefineInfo
    {
        public int carpoiid;
        public int poiid;
        public string starttime;
        public string endtime;
        public int always;
        public string weekdates;
        public string poidate;
        public string poiname;
    }
}
