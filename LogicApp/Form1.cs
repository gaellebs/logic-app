﻿using Microsoft.Reporting.WinForms;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml;

namespace LogicApp
{
    public partial class Form1 : Form
    {
        #region Private Fields
        private int num = 0;
        private int sid = 0;
        private int clock = 0;
        private bool backup = false;
        private Hashtable carht = new Hashtable();
        private Hashtable caridht = new Hashtable();
        private Hashtable companyht = new Hashtable();
        private Hashtable fueldefineht = new Hashtable();
        private Hashtable statusdefineht = new Hashtable();
        private Hashtable alarmdefineht = new Hashtable();
        private Hashtable fencedefineht = new Hashtable();
        private Hashtable fencespeeddefineht = new Hashtable();
        private Hashtable fencecutoildefineht = new Hashtable();
        private Hashtable driverht = new Hashtable();
        private Hashtable cardriverht = new Hashtable();
        private Hashtable districtht = new Hashtable();
        private Hashtable blockht = new Hashtable();
        private Hashtable stackht = new Hashtable();
        private Hashtable statusht = new Hashtable();
        private Hashtable addressht = new Hashtable();
        private Hashtable fuelalarmht = new Hashtable();
        private bool handleing = false;
        private bool transfering = false;
        private Hashtable tracklistfenceht = new Hashtable();
        private Hashtable poiinfoht = new Hashtable();
        private Hashtable poiht = new Hashtable();
        private Hashtable carpoiht = new Hashtable();
        private double mindistance = 200.0;
        private IContainer mycomponents = (IContainer)null;
        private System.Threading.Timer recordTimer;
        private Label LabNum;
        private Button button1;
        private ReportViewer reportViewer1;
        private CheckBox ChkScan;
        private Button button2;
        private System.Windows.Forms.Timer ClockTimer;
        private System.Windows.Forms.Timer TogetherTimer;
        private System.Windows.Forms.Timer ScanTimer;
        private System.Windows.Forms.Timer STTimer;
        private Button button5;
        private TextBox textBox1;
        private Button button3;
        private TextBox textBox2;
        #endregion

        public Form1()
        {
            InitializeComponent();
            this.MyInitializeComponent();
            this.IntialSaveRecord();
        }

        private void IntialSaveRecord()
        {
            AutoResetEvent autoResetEvent = new AutoResetEvent(false);
            //Initialiaze our data from the database into HashTables
            this.InitHashtable();
            this.recordTimer = new System.Threading.Timer(new TimerCallback(this.CallbackTask), (object)autoResetEvent, 0, 5000);
        }

        private void ClockCallbackTask(object stateInfo)
        {
            DateTime now = DateTime.Now;
            if (now.ToString("HH:mm:ss").Equals("09:13:30"))
            {
                using (DataTable table = SqlHelper.ExecuteDataset("select * from remind where state=0").Tables[0])
                {
                    foreach (DataRow row in (InternalDataCollectionBase)table.Rows)
                    {
                        int num1 = (int)row["RemindKindID"];
                        bool flag = false;
                        string str1 = "";
                        string str2 = "";
                        int num2 = 0;
                        using (SqlDataReader sqlDataReader = SqlHelper.ExecuteReader("select * from vcar where id=" + row["carid"]))
                        {
                            if (sqlDataReader.Read())
                            {
                                str1 = sqlDataReader["ImeiNo"].ToString();
                                str2 = sqlDataReader["Plate"].ToString();
                                num2 = (int)sqlDataReader["CompanyID"];
                            }
                        }
                        switch (num1)
                        {
                            case 2:
                                double num3 = (double)row["Odometer"];
                                using (SqlDataReader sqlDataReader = SqlHelper.ExecuteReader("select top 1 * from tracklist_tmp where imeino='" + str1 + "' order by createtime desc"))
                                {
                                    if (sqlDataReader.Read())
                                    {
                                        if ((double)sqlDataReader["Mileage"] >= num3)
                                        {
                                            flag = true;
                                            SqlHelper.ExecuteNonQuery("update remind set state=1,remindtime='" + now.ToString("yyyy-MM-dd HH:mm:ss") + "' where id=" + row["ID"]);
                                        }
                                        break;
                                    }
                                    break;
                                }
                            case 3:
                                DateTime dateTime = DateTime.Parse(row["RemindDate"].ToString());
                                if (now.CompareTo(dateTime) >= 0)
                                {
                                    flag = true;
                                    SqlHelper.ExecuteNonQuery("update remind set state=1,remindtime='" + now.ToString("yyyy-MM-dd HH:mm:ss") + "' where id=" + row["ID"]);
                                }
                                break;
                        }
                        if (flag && !row["NotifyEmail"].ToString().Trim().Equals("") && num2 != 0)
                        {
                            using (SqlDataReader sqlDataReader = SqlHelper.ExecuteReader("select * from company where id=" + (object)num2))
                            {
                                if (sqlDataReader.Read())
                                {
                                    string host = sqlDataReader["EmailHost"].ToString();
                                    string port = sqlDataReader["EmailPort"].ToString();
                                    string username = sqlDataReader["EmailUsername"].ToString();
                                    string password = sqlDataReader["EmailPassword"].ToString();
                                    string address = sqlDataReader["EmailAddress"].ToString();
                                    if (!host.Equals("") && !port.Equals("") && (!username.Equals("") && !password.Equals("")) && !address.Equals(""))
                                    {
                                        string[] strArray = row["NotifyEmail"].ToString().Trim().Split(',');
                                        for (int index = 0; index < strArray.Length; ++index)
                                        {
                                            try
                                            {
                                                FileOpetation.SaveRecord("Sending Notify Email To " + strArray[index].Trim() + "\r\n");
                                                Utils.SendEmail(host, port, username, password, address, "Reminder-" + str2 + "(" + str1 + ")", row["RemindMemo"].ToString(), strArray[index].Trim());
                                            }
                                            catch (Exception ex)
                                            {
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (now.ToString("HH:mm:ss").Equals("14:17:00"))
                //this.CheckScheduleReport();
            if (now.ToString("HH:mm").Equals("09:30"))
                this.backup = false;
            if (now.ToString("HH:mm").Equals("09:35") && this.backup)
            {
                this.backup = false;
                FileOpetation.SaveRecord("Backuping......\r\n");
                HSSFWorkbook hssfworkbook = new HSSFWorkbook();
                this.CreateExcelBySQL(hssfworkbook, "company", "select * from company");
                this.CreateExcelBySQL(hssfworkbook, "team", "select * from team");
                this.CreateExcelBySQL(hssfworkbook, "car", "select * from car");
                this.CreateExcelBySQL(hssfworkbook, "driver", "select * from driver");
                this.CreateExcelBySQL(hssfworkbook, "tb_action", "select * from tb_action");
                this.CreateExcelBySQL(hssfworkbook, "tb_menu", "select * from tb_menu");
                this.CreateExcelBySQL(hssfworkbook, "tb_role", "select * from tb_role");
                this.CreateExcelBySQL(hssfworkbook, "tb_roleaction", "select * from tb_roleaction");
                this.CreateExcelBySQL(hssfworkbook, "tb_rolemenu", "select * from tb_rolemenu");
                this.CreateExcelBySQL(hssfworkbook, "tb_user", "select * from tb_user");
                this.CreateExcelBySQL(hssfworkbook, "tb_useraction", "select * from tb_useraction");
                this.CreateExcelBySQL(hssfworkbook, "tb_userdata", "select * from tb_userdata");
                this.CreateExcelBySQL(hssfworkbook, "tb_usermenu", "select * from tb_usermenu");
                this.CreateExcelBySQL(hssfworkbook, "tb_userrole", "select * from tb_userrole");
                this.CreateExcelBySQL(hssfworkbook, "usercompany", "select * from usercompany");
                this.CreateExcelBySQL(hssfworkbook, "userteam", "select * from userteam");
                this.CreateExcelBySQL(hssfworkbook, "usercar", "select * from usercar");
                this.CreateExcelBySQL(hssfworkbook, "userrole", "select * from userrole");
                this.CreateExcelBySQL(hssfworkbook, "useruser", "select * from useruser");
                this.CreateExcelBySQL(hssfworkbook, "userextend", "select * from userextend");
                using (FileStream fileStream = new FileStream("Backup/backup_" + now.ToString("yyyyMMddHHmmss") + ".xls", FileMode.Create))
                {
                    hssfworkbook.Write((Stream)fileStream);
                    fileStream.Close();
                    fileStream.Dispose();
                }
                Utils.SendAttachmentEmail("smtp.163.com", "25", "backupgps@163.com", "madchlwy123456", "backupgps@163.com", "Backup(192.126.123.17)-" + now.ToString("yyyyMMddHHmmss"), "数据备份邮件!", "1758943@qq.com", "Backup/backup_" + now.ToString("yyyyMMddHHmmss") + ".xls");
            }
            if (!this.fuelalarmht.ContainsKey((object)now.AddMinutes(-3.0).ToString("yyyyMMddHHmm")))
                return;
            this.fuelalarmht.Remove((object)now.AddMinutes(-3.0).ToString("yyyyMMddHHmm"));
        }

        private void OtherCallbackTask(object stateInfo)
        {
            FileOpetation.SaveRecord(string.Format("Call 5 Min\r\n", (object)DateTime.Now));
            this.InitHashtable();
            FileOpetation.SaveRecord(string.Format("XXXXXXXXXXXXXXXXXX", (object)DateTime.Now));
            foreach (DictionaryEntry dictionaryEntry in Utils.companyspeedht)
            {
                ArrayList arrayList1 = (ArrayList)dictionaryEntry.Value;
                CompanyInfo companyInfo = (CompanyInfo)this.companyht[(object)dictionaryEntry.Key.ToString()];
                FileOpetation.SaveRecord(string.Format("CompanyID:" + dictionaryEntry.Key.ToString() + "|Count:" + (object)arrayList1.Count + "|" + (object)companyInfo.togethercnt));
                if (companyInfo.togethercnt > 0 && arrayList1.Count >= companyInfo.togethercnt)
                {
                    ArrayList arrayList2 = new ArrayList();
                    for (int index1 = 0; index1 < arrayList1.Count - 1; ++index1)
                    {
                        if (!arrayList2.Contains(arrayList1[index1]))
                        {
                            int num1 = 0;
                            string str1 = "";
                            string str2 = "";
                            string str3 = "";
                            TrackListInfo trackListInfo1 = (TrackListInfo)Utils.tracklistht[arrayList1[index1]];
                            for (int index2 = index1 + 1; index2 < arrayList1.Count; ++index2)
                            {
                                TrackListInfo trackListInfo2 = (TrackListInfo)Utils.tracklistht[arrayList1[index2]];
                                double num2 = Utils.GetDistance(double.Parse(trackListInfo1.latitude), double.Parse(trackListInfo1.longitude), double.Parse(trackListInfo2.latitude), double.Parse(trackListInfo2.longitude)) * 1000.0;
                                FileOpetation.SaveRecord(string.Format("Distance(" + arrayList1[index1] + "|" + arrayList1[index2] + "):" + (object)num2));
                                if (num2 < companyInfo.togetherdistance)
                                {
                                    ++num1;
                                    if (str1.Equals(""))
                                    {
                                        str2 = str2 + arrayList1[index1] + ",";
                                        CarInfo carInfo = (CarInfo)this.carht[arrayList1[index1]];
                                        str1 = str1 + (object)carInfo.carid + ",";
                                        str3 = str3 + carInfo.plate + ",";
                                    }
                                    str2 = str2 + arrayList1[index2] + ",";
                                    CarInfo carInfo1 = (CarInfo)this.carht[arrayList1[index2]];
                                    str1 = str1 + (object)carInfo1.carid + ",";
                                    str3 = str3 + carInfo1.plate + ",";
                                }
                            }
                            if (num1 >= companyInfo.togethercnt - 1)
                            {
                                StringBuilder stringBuilder = new StringBuilder();
                                stringBuilder.Append("insert into togetheralarm(");
                                stringBuilder.Append("createtime,carids,imeinos,plates,companyid)");
                                stringBuilder.Append(" values (");
                                stringBuilder.Append("@createtime,@carids,@imeinos,@plates,@companyid)");
                                SqlParameter[] sqlParameterArray = new SqlParameter[5]
                                {
                  new SqlParameter("@createtime", SqlDbType.DateTime),
                  new SqlParameter("@carids", SqlDbType.VarChar, 500),
                  new SqlParameter("@imeinos", SqlDbType.VarChar, 1000),
                  new SqlParameter("@plates", SqlDbType.VarChar, 2000),
                  new SqlParameter("@companyid", SqlDbType.Int, 4)
                                };
                                sqlParameterArray[0].Value = (object)DateTime.Now;
                                sqlParameterArray[1].Value = (object)str1.TrimEnd(',');
                                sqlParameterArray[2].Value = (object)str2.TrimEnd(',');
                                sqlParameterArray[3].Value = (object)str3.TrimEnd(',');
                                sqlParameterArray[4].Value = (object)int.Parse(dictionaryEntry.Key.ToString());
                                SqlHelper.ExecuteNonQuery(stringBuilder.ToString(), sqlParameterArray);
                                string str4 = str2.TrimEnd(',');
                                char[] chArray = new char[1] { ',' };
                                foreach (object obj in str4.Split(chArray))
                                    arrayList2.Add(obj);
                            }
                        }
                    }
                }
            }
        }

        private void InitHashtable()
        {
            try
            {
                FileOpetation.SaveRecord(string.Format("Initialization begins...", (object)DateTime.Now));
                this.carht = new Hashtable();
                this.caridht = new Hashtable();
                this.companyht = new Hashtable();
                this.fueldefineht = new Hashtable();
                this.statusdefineht = new Hashtable();
                this.alarmdefineht = new Hashtable();
                this.fencedefineht = new Hashtable();
                this.fencespeeddefineht = new Hashtable();
                this.driverht = new Hashtable();
                this.cardriverht = new Hashtable();
                this.districtht = new Hashtable();
                this.blockht = new Hashtable();
                this.stackht = new Hashtable();
                this.fencecutoildefineht = new Hashtable();
                this.poiinfoht = new Hashtable();
                this.poiht = new Hashtable();
                this.carpoiht = new Hashtable();


                //Fetching all companies that are active
                using (SqlDataReader sqlDataReader = SqlHelper.ExecuteReader("select A.*,B.TogetherCnt,B.TogetherDistance from Company A left join CompanyDefine B on A.ID=B.CompanyID where A.State=0"))
                {
                    while (sqlDataReader.Read())
                    {
                        CompanyInfo companyInfo = new CompanyInfo();
                        companyInfo.togethercnt = (int)sqlDataReader["TogetherCnt"];
                        companyInfo.togetherdistance = (double)sqlDataReader["TogetherDistance"];
                        companyInfo.emailhost = sqlDataReader["EmailHost"].ToString();
                        companyInfo.emailport = sqlDataReader["EmailPort"].ToString();
                        companyInfo.emailusername = sqlDataReader["EmailUsername"].ToString();
                        companyInfo.emailpassword = sqlDataReader["EmailPassword"].ToString();
                        companyInfo.emailaddress = sqlDataReader["EmailAddress"].ToString();
                        try
                        {
                            this.companyht.Add((object)sqlDataReader["ID"].ToString().Trim(), (object)companyInfo);
                        }
                        catch (Exception ex)
                        {
                            FileOpetation.SaveRecord(string.Format("Initialization exception:" + ex.Message + "(" + ex.StackTrace.Substring(ex.StackTrace.IndexOf("line") + 4).Trim() + ")\r\n", (object)DateTime.Now));
                        }
                    }
                }
                FileOpetation.SaveRecord("Initialize the company\r\n");
                //Fetch all the cars from databse with State = 0
                using (SqlDataReader sqlDataReader = SqlHelper.ExecuteReader("select * from vCar where state=0"))
                {
                    while (sqlDataReader.Read())
                    {
                        CarInfo carInfo = new CarInfo();
                        carInfo.carid = (int)sqlDataReader["ID"];
                        carInfo.imeino = sqlDataReader["ImeiNo"].ToString().Trim();
                        carInfo.plate = sqlDataReader["Plate"].ToString();
                        carInfo.protocol = (int)sqlDataReader["ProtocolID"];
                        carInfo.timezone = (double)sqlDataReader["TimeZone"];
                        carInfo.companyid = (int)sqlDataReader["CompanyID"];
                        carInfo.state = (int)sqlDataReader["State"];
                        carInfo.mapinfo = sqlDataReader["mapinfo"].ToString();
                        carInfo.basemileage = (double)sqlDataReader["BaseMileage"];
                        carInfo.engineno = sqlDataReader["EngineNo"].ToString();
                        try
                        {
                            this.carht.Add((object)sqlDataReader["imeino"].ToString().Trim(), (object)carInfo);
                            this.caridht.Add((object)(int)sqlDataReader["ID"], (object)carInfo);
                        }
                        catch (Exception ex)
                        {
                            FileOpetation.SaveRecord(string.Format("Initialization exception:" + ex.Message + "(" + ex.StackTrace.Substring(ex.StackTrace.IndexOf("line") + 4).Trim() + ")\r\n", (object)DateTime.Now));
                        }
                    }
                }
                FileOpetation.SaveRecord("Initialize the vehicle\r\n");
                //Get Specific car fuel define info
                using (SqlDataReader sqlDataReader = SqlHelper.ExecuteReader("select * from FuelDefine"))
                {
                    while (sqlDataReader.Read())
                    {
                        FuelDefineInfo fuelDefineInfo = new FuelDefineInfo();
                        fuelDefineInfo.quart1 = (int)sqlDataReader["Quart1"];
                        fuelDefineInfo.maxfuel1 = (int)sqlDataReader["MaxFuel1"];
                        fuelDefineInfo.minfuel1 = (int)sqlDataReader["MinFuel1"];
                        fuelDefineInfo.quart2 = (int)sqlDataReader["Quart2"];
                        fuelDefineInfo.maxfuel2 = (int)sqlDataReader["MaxFuel2"];
                        fuelDefineInfo.minfuel2 = (int)sqlDataReader["MinFuel2"];
                        try
                        {
                            this.fueldefineht.Add((object)(int)sqlDataReader["CarID"], (object)fuelDefineInfo);
                        }
                        catch (Exception ex)
                        {
                            FileOpetation.SaveRecord(string.Format("Initialiaze Exception:" + ex.Message + "(" + ex.StackTrace.Substring(ex.StackTrace.IndexOf("line") + 4).Trim() + ")\r\n", (object)DateTime.Now));
                        }
                    }
                }
                //Get StatusDefine for each car output data bla bla bla
                FileOpetation.SaveRecord("Initialize the amount of oil\r\n");
                using (SqlDataReader sqlDataReader = SqlHelper.ExecuteReader("select * from StatusDefine"))
                {
                    while (sqlDataReader.Read())
                    {
                        StatusDefineInfo statusDefineInfo = new StatusDefineInfo();
                        if (sqlDataReader["Output1On"] != null)
                            statusDefineInfo.output1on = sqlDataReader["Output1On"].ToString();
                        if (sqlDataReader["Output2On"] != null)
                            statusDefineInfo.output2on = sqlDataReader["Output2On"].ToString();
                        if (sqlDataReader["Output3On"] != null)
                            statusDefineInfo.output3on = sqlDataReader["Output3On"].ToString();
                        if (sqlDataReader["Output4On"] != null)
                            statusDefineInfo.output4on = sqlDataReader["Output4On"].ToString();
                        if (sqlDataReader["Output5On"] != null)
                            statusDefineInfo.output5on = sqlDataReader["Output5On"].ToString();
                        if (sqlDataReader["Output1Off"] != null)
                            statusDefineInfo.output1off = sqlDataReader["Output1Off"].ToString();
                        if (sqlDataReader["Output2Off"] != null)
                            statusDefineInfo.output2off = sqlDataReader["Output2Off"].ToString();
                        if (sqlDataReader["Output3Off"] != null)
                            statusDefineInfo.output3off = sqlDataReader["Output3Off"].ToString();
                        if (sqlDataReader["Output4Off"] != null)
                            statusDefineInfo.output4off = sqlDataReader["Output4Off"].ToString();
                        if (sqlDataReader["Output5Off"] != null)
                            statusDefineInfo.output5off = sqlDataReader["Output5Off"].ToString();
                        if (sqlDataReader["Input1On"] != null)
                            statusDefineInfo.input1on = sqlDataReader["Input1On"].ToString();
                        if (sqlDataReader["Input2On"] != null)
                            statusDefineInfo.input2on = sqlDataReader["Input2On"].ToString();
                        if (sqlDataReader["Input3On"] != null)
                            statusDefineInfo.input3on = sqlDataReader["Input3On"].ToString();
                        if (sqlDataReader["Input4On"] != null)
                            statusDefineInfo.input4on = sqlDataReader["Input4On"].ToString();
                        if (sqlDataReader["Input5On"] != null)
                            statusDefineInfo.input5on = sqlDataReader["Input5On"].ToString();
                        if (sqlDataReader["Input1Off"] != null)
                            statusDefineInfo.input1off = sqlDataReader["Input1Off"].ToString();
                        if (sqlDataReader["Input2Off"] != null)
                            statusDefineInfo.input2off = sqlDataReader["Input2Off"].ToString();
                        if (sqlDataReader["Input3Off"] != null)
                            statusDefineInfo.input3off = sqlDataReader["Input3Off"].ToString();
                        if (sqlDataReader["Input4Off"] != null)
                            statusDefineInfo.input4off = sqlDataReader["Input4Off"].ToString();
                        if (sqlDataReader["Input5Off"] != null)
                            statusDefineInfo.input5off = sqlDataReader["Input5Off"].ToString();
                        try
                        {
                            this.statusdefineht.Add((object)(int)sqlDataReader["CarID"], (object)statusDefineInfo);
                        }
                        catch (Exception ex)
                        {
                            FileOpetation.SaveRecord(string.Format("Initialiaze Exception:" + ex.Message + "(" + ex.StackTrace.Substring(ex.StackTrace.IndexOf("line") + 4).Trim() + ")\r\n", (object)DateTime.Now));
                        }
                    }
                }
                //Alaram Define + Alarm Code of specific car
                FileOpetation.SaveRecord("Initialization state\r\n");
                using (SqlDataReader sqlDataReader = SqlHelper.ExecuteReader("select A.*,M.ImeiNo,M.TogetherMinute,C.Name as SInput1On,D.Name as SInput2On,E.Name as SInput3On,F.Name as SInput4On,G.Name as SInput5On,H.Name as SInput1Off,I.Name as SInput2Off,J.Name as SInput3Off,K.Name as SInput4Off,L.Name as SInput5Off from AlarmDefine A left join AlarmCode C on A.Input1On=C.ID left join AlarmCode D on A.Input2On=D.ID left join AlarmCode E on A.Input3On=E.ID left join AlarmCode F on A.Input4On=F.ID left join AlarmCode G on A.Input5On=G.ID left join AlarmCode H on A.Input1Off=H.ID left join AlarmCode I on A.Input2Off=I.ID left join AlarmCode J on A.Input3Off=J.ID left join AlarmCode K on A.Input4Off=K.ID left join AlarmCode L on A.Input5Off=L.ID left join vCar M on A.CarID=M.ID"))
                {
                    while (sqlDataReader.Read())
                    {
                        AlarmDefineInfo alarmDefineInfo = new AlarmDefineInfo();
                        if (sqlDataReader["Input1On"] != null)
                        {
                            alarmDefineInfo.input1on = (int)sqlDataReader["Input1On"];
                            alarmDefineInfo.sinput1on = sqlDataReader["SInput1On"].ToString();
                        }
                        if (sqlDataReader["Input2On"] != null)
                        {
                            alarmDefineInfo.input2on = (int)sqlDataReader["Input2On"];
                            alarmDefineInfo.sinput2on = sqlDataReader["SInput2On"].ToString();
                        }
                        if (sqlDataReader["Input3On"] != null)
                        {
                            alarmDefineInfo.input3on = (int)sqlDataReader["Input3On"];
                            alarmDefineInfo.sinput3on = sqlDataReader["SInput3On"].ToString();
                        }
                        if (sqlDataReader["Input4On"] != null)
                        {
                            alarmDefineInfo.input4on = (int)sqlDataReader["Input4On"];
                            alarmDefineInfo.sinput4on = sqlDataReader["SInput4On"].ToString();
                        }
                        if (sqlDataReader["Input5On"] != null)
                        {
                            alarmDefineInfo.input5on = (int)sqlDataReader["Input5On"];
                            alarmDefineInfo.sinput5on = sqlDataReader["SInput5On"].ToString();
                        }
                        if (sqlDataReader["Input1Off"] != null)
                        {
                            alarmDefineInfo.input1off = (int)sqlDataReader["Input1Off"];
                            alarmDefineInfo.sinput1off = sqlDataReader["SInput1Off"].ToString();
                        }
                        if (sqlDataReader["Input2Off"] != null)
                        {
                            alarmDefineInfo.input2off = (int)sqlDataReader["Input2Off"];
                            alarmDefineInfo.sinput2off = sqlDataReader["SInput2Off"].ToString();
                        }
                        if (sqlDataReader["Input3Off"] != null)
                        {
                            alarmDefineInfo.input3off = (int)sqlDataReader["Input3Off"];
                            alarmDefineInfo.sinput3off = sqlDataReader["SInput3Off"].ToString();
                        }
                        if (sqlDataReader["Input4Off"] != null)
                        {
                            alarmDefineInfo.input4off = (int)sqlDataReader["Input4Off"];
                            alarmDefineInfo.sinput4off = sqlDataReader["SInput4Off"].ToString();
                        }
                        if (sqlDataReader["Input5Off"] != null)
                        {
                            alarmDefineInfo.input5off = (int)sqlDataReader["Input5Off"];
                            alarmDefineInfo.sinput5off = sqlDataReader["SInput5Off"].ToString();
                        }
                        if (sqlDataReader["Speed"] != null)
                            alarmDefineInfo.speed = (double)sqlDataReader["Speed"];
                        if (sqlDataReader["Engine"] != null)
                            alarmDefineInfo.engine = (int)sqlDataReader["Engine"];
                        if (sqlDataReader["Working"] != null)
                            alarmDefineInfo.working = (int)sqlDataReader["Working"];
                        if (sqlDataReader["MoveMinute"] != null)
                            alarmDefineInfo.moveminute = (double)sqlDataReader["MoveMinute"];
                        if (sqlDataReader["MoveSpeed"] != null)
                            alarmDefineInfo.movespeed = (double)sqlDataReader["MoveSpeed"];
                        if (sqlDataReader["MoveInterval"] != null)
                            alarmDefineInfo.moveinterval = (double)sqlDataReader["MoveInterval"];
                        if (sqlDataReader["TogetherMinute"] != null)
                            alarmDefineInfo.togetherminute = (double)sqlDataReader["TogetherMinute"];
                        if (sqlDataReader["EngineOn"] != null)
                            alarmDefineInfo.engineon = (int)sqlDataReader["EngineOn"];
                        try
                        {
                            this.alarmdefineht.Add((object)(int)sqlDataReader["CarID"], (object)alarmDefineInfo);
                        }
                        catch (Exception ex)
                        {
                            FileOpetation.SaveRecord(string.Format("Initialiaze Exception:" + ex.Message + "(" + ex.StackTrace.Substring(ex.StackTrace.IndexOf("line") + 4).Trim() + ")\r\n", (object)DateTime.Now));
                        }
                    }
                }
                //Car fence and geofence location
                FileOpetation.SaveRecord("Initialize alarm\r\n");
                using (SqlDataReader sqlDataReader = SqlHelper.ExecuteReader("select *,A.ID as VID from vCarFence A left join GeoFence B on A.GeoFenceID=B.ID where State=0"))
                {
                    while (sqlDataReader.Read())
                    {
                        FenceDefineInfo fenceDefineInfo = new FenceDefineInfo();
                        fenceDefineInfo.carfenceid = (int)sqlDataReader["VID"];
                        fenceDefineInfo.always = (int)sqlDataReader["Always"];
                        fenceDefineInfo.endtime = sqlDataReader["EndTime"].ToString();
                        fenceDefineInfo.fencedate = sqlDataReader["FenceDate"].ToString();
                        fenceDefineInfo.fencename = sqlDataReader["Name"].ToString();
                        fenceDefineInfo.geofenceid = (int)sqlDataReader["GeoFenceID"];
                        fenceDefineInfo.geofencekindid = (int)sqlDataReader["GeoFenceKindID"];
                        fenceDefineInfo.inorout = (int)sqlDataReader["InOrOut"];
                        fenceDefineInfo.latlngs = sqlDataReader["LatLngs"].ToString();
                        fenceDefineInfo.radius = (double)sqlDataReader["Radius"];
                        fenceDefineInfo.starttime = sqlDataReader["StartTime"].ToString();
                        fenceDefineInfo.weekdates = sqlDataReader["WeekDates"].ToString();
                        fenceDefineInfo.cutoil = (int)sqlDataReader["CutOil"];
                        int num = (int)sqlDataReader["CarID"];
                        try
                        {
                            if (this.fencedefineht.ContainsKey((object)num))
                                ((ArrayList)this.fencedefineht[(object)num]).Add((object)fenceDefineInfo);
                            else
                                this.fencedefineht.Add((object)num, (object)new ArrayList()
                {
                  (object) fenceDefineInfo
                });
                        }
                        catch (Exception ex)
                        {
                            FileOpetation.SaveRecord(string.Format("Initialiaze Exception:" + ex.Message + "(" + ex.StackTrace.Substring(ex.StackTrace.IndexOf("line") + 4).Trim() + ")\r\n", (object)DateTime.Now));
                        }
                    }
                }
                //Area safedefence with geofence of the car
                FileOpetation.SaveRecord("Initialize the fence\r\n");
                using (SqlDataReader sqlDataReader = SqlHelper.ExecuteReader("select * from areaspeeddefine A left join GeoFence B on A.geofenceid=B.id where A.state=0 and A.GeoFenceID!=-1"))
                {
                    while (sqlDataReader.Read())
                    {
                        FenceSpeedDefineInfo fenceSpeedDefineInfo = new FenceSpeedDefineInfo();
                        fenceSpeedDefineInfo.datetype = (int)sqlDataReader["datetype"];
                        fenceSpeedDefineInfo.endtime = sqlDataReader["EndTime"].ToString();
                        fenceSpeedDefineInfo.areadate = sqlDataReader["AreaDate"].ToString();
                        fenceSpeedDefineInfo.fencename = sqlDataReader["Name"].ToString();
                        fenceSpeedDefineInfo.geofenceid = (int)sqlDataReader["GeoFenceID"];
                        fenceSpeedDefineInfo.geofencekindid = (int)sqlDataReader["GeoFenceKindID"];
                        fenceSpeedDefineInfo.speed = (double)sqlDataReader["Speed"];
                        fenceSpeedDefineInfo.latlngs = sqlDataReader["LatLngs"].ToString();
                        fenceSpeedDefineInfo.radius = (double)sqlDataReader["Radius"];
                        fenceSpeedDefineInfo.starttime = sqlDataReader["StartTime"].ToString();
                        fenceSpeedDefineInfo.weekdates = sqlDataReader["WeekDates"].ToString();
                        int num = (int)sqlDataReader["CarID"];
                        try
                        {
                            if (this.fencespeeddefineht.ContainsKey((object)num))
                                ((ArrayList)this.fencespeeddefineht[(object)num]).Add((object)fenceSpeedDefineInfo);
                            else
                                this.fencespeeddefineht.Add((object)num, (object)new ArrayList()
                {
                  (object) fenceSpeedDefineInfo
                });
                        }
                        catch (Exception ex)
                        {
                            FileOpetation.SaveRecord(string.Format("Initialiaze Exception:" + ex.Message + "(" + ex.StackTrace.Substring(ex.StackTrace.IndexOf("line") + 4).Trim() + ")\r\n", (object)DateTime.Now));
                        }
                    }
                }
                //Area cut oil with Geofence of a car
                FileOpetation.SaveRecord("Initialization speed fence\r\n");
                using (SqlDataReader sqlDataReader = SqlHelper.ExecuteReader("select * from areacutoildefine A left join GeoFence B on A.geofenceid=B.id where A.state=0"))
                {
                    while (sqlDataReader.Read())
                    {
                        FenceCutOilDefineInfo cutOilDefineInfo = new FenceCutOilDefineInfo();
                        cutOilDefineInfo.datetype = (int)sqlDataReader["datetype"];
                        cutOilDefineInfo.endtime = sqlDataReader["EndTime"].ToString();
                        cutOilDefineInfo.areadate = sqlDataReader["AreaDate"].ToString();
                        cutOilDefineInfo.fencename = sqlDataReader["Name"].ToString();
                        cutOilDefineInfo.geofenceid = (int)sqlDataReader["GeoFenceID"];
                        cutOilDefineInfo.geofencekindid = (int)sqlDataReader["GeoFenceKindID"];
                        cutOilDefineInfo.inorout = (int)sqlDataReader["InOrOut"];
                        cutOilDefineInfo.latlngs = sqlDataReader["LatLngs"].ToString();
                        cutOilDefineInfo.radius = (double)sqlDataReader["Radius"];
                        cutOilDefineInfo.starttime = sqlDataReader["StartTime"].ToString();
                        cutOilDefineInfo.weekdates = sqlDataReader["WeekDates"].ToString();
                        int num = (int)sqlDataReader["CarID"];
                        try
                        {
                            if (this.fencecutoildefineht.ContainsKey((object)num))
                                ((ArrayList)this.fencecutoildefineht[(object)num]).Add((object)cutOilDefineInfo);
                            else
                                this.fencecutoildefineht.Add((object)num, (object)new ArrayList()
                {
                  (object) cutOilDefineInfo
                });
                        }
                        catch (Exception ex)
                        {
                            FileOpetation.SaveRecord(string.Format("Initialiaze Exception:" + ex.Message + "(" + ex.StackTrace.Substring(ex.StackTrace.IndexOf("line") + 4).Trim() + ")\r\n", (object)DateTime.Now));
                        }
                    }
                }
                //Get all active drivers
                FileOpetation.SaveRecord("Initialize the fence to break the oil\r\n");
                using (SqlDataReader sqlDataReader = SqlHelper.ExecuteReader("select * from driver where state=0"))
                {
                    while (sqlDataReader.Read())
                    {
                        this.driverht.Add((object)sqlDataReader["RFID"].ToString(), (object)(int)sqlDataReader["ID"]);
                        if (sqlDataReader["CarID"] != DBNull.Value && !this.cardriverht.ContainsKey((object)(int)sqlDataReader["CarID"]))
                        {
                            try
                            {
                                this.cardriverht.Add((object)(int)sqlDataReader["CarID"], (object)(int)sqlDataReader["ID"]);
                            }
                            catch (Exception ex)
                            {
                                FileOpetation.SaveRecord(string.Format("Initialiaze Exception:" + ex.Message + "(" + ex.StackTrace.Substring(ex.StackTrace.IndexOf("line") + 4).Trim() + ")\r\n", (object)DateTime.Now));
                            }
                        }
                    }
                }
                FileOpetation.SaveRecord("Initialize the driver\r\n");
                FileOpetation.SaveRecord("Initialization area\r\n");
                //Get Car location from trackerList logitude and altitude
                if (this.addressht.Count == 0)
                {
                    using (SqlDataReader sqlDataReader = SqlHelper.ExecuteReader("select A.Latitude,A.Longitude,A.Address,B.ID from TrackList_Tmp A with(nolock) left join Car B on A.ImeiNo=B.ImeiNo"))
                    {
                        while (sqlDataReader.Read())
                        {
                            if (sqlDataReader["ID"] != DBNull.Value && sqlDataReader["Address"] != DBNull.Value && !sqlDataReader["Address"].ToString().Equals(""))
                            {
                                int num = (int)sqlDataReader["ID"];
                                this.addressht.Remove((object)num);
                                this.addressht.Add((object)num, (object)(sqlDataReader["Latitude"].ToString() + "|" + sqlDataReader["Longitude"].ToString() + "|" + sqlDataReader["Address"].ToString()));
                            }
                        }
                    }
                }
                FileOpetation.SaveRecord("Initial address\r\n");
                //Select Point of Interest active ones
                using (SqlDataReader sqlDataReader = SqlHelper.ExecuteReader("select * from poi where state=0"))
                {
                    while (sqlDataReader.Read())
                    {
                        if (!sqlDataReader["Latitude"].ToString().Equals("") && !sqlDataReader["Longitude"].ToString().Equals(""))
                        {
                            POIInfo poiInfo = new POIInfo();
                            poiInfo.id = (int)sqlDataReader["ID"];
                            poiInfo.name = sqlDataReader["Name"].ToString();
                            poiInfo.companyid = (int)sqlDataReader["CompanyID"];
                            poiInfo.poitypeid = (int)sqlDataReader["POITypeID"];
                            try
                            {
                                poiInfo.latitude = double.Parse(sqlDataReader["Latitude"].ToString());
                                poiInfo.longitude = double.Parse(sqlDataReader["Longitude"].ToString());
                                poiInfo.radius = (double)sqlDataReader["Radius"];
                            }
                            catch (Exception ex)
                            {
                                FileOpetation.SaveRecord(poiInfo.id.ToString());
                            }
                            if (this.poiinfoht.ContainsKey((object)poiInfo.companyid))
                                ((ArrayList)this.poiinfoht[(object)poiInfo.companyid]).Add((object)poiInfo);
                            else
                                this.poiinfoht.Add((object)poiInfo.companyid, (object)new ArrayList()
                {
                  (object) poiInfo
                });
                        }
                    }
                }
                //Select Point of Interest Tracklist
                using (SqlDataReader sqlDataReader = SqlHelper.ExecuteReader("select * from tracklistpoi where state=0"))
                {
                    while (sqlDataReader.Read())
                    {
                        if (!this.poiht.ContainsKey((object)(sqlDataReader["ImeiNo"].ToString() + "-" + sqlDataReader["POIID"].ToString())))
                            this.poiht.Add((object)(sqlDataReader["ImeiNo"].ToString() + "-" + sqlDataReader["POIID"].ToString()), (object)new TrackListPOIInfo()
                            {
                                imeino = sqlDataReader["ImeiNO"].ToString(),
                                squaddate = DateTime.Parse(sqlDataReader["SquadDate"].ToString()),
                                roundrouteid = 0,
                                routeid = 0,
                                siteid = 0,
                                poiid = int.Parse(sqlDataReader["POIID"].ToString()),
                                enterlatitude = sqlDataReader["EnterLatitude"].ToString(),
                                enterlongitude = sqlDataReader["EnterLongitude"].ToString(),
                                entermileage = (double)sqlDataReader["EnterMileage"],
                                entertime = DateTime.Parse(sqlDataReader["EnterTime"].ToString()),
                                kind = "UnPlan"
                            });
                    }
                }
                FileOpetation.SaveRecord("Initialize POI\r\n");
                //Car Point of interest
                using (SqlDataReader sqlDataReader = SqlHelper.ExecuteReader("select * from vCarPOI"))
                {
                    while (sqlDataReader.Read())
                    {
                        POIDefineInfo poiDefineInfo = new POIDefineInfo();
                        poiDefineInfo.carpoiid = (int)sqlDataReader["ID"];
                        poiDefineInfo.always = (int)sqlDataReader["Always"];
                        poiDefineInfo.endtime = sqlDataReader["EndTime"].ToString();
                        poiDefineInfo.poidate = sqlDataReader["POIDate"].ToString();
                        poiDefineInfo.poiname = sqlDataReader["Name"].ToString();
                        poiDefineInfo.poiid = (int)sqlDataReader["POIID"];
                        poiDefineInfo.starttime = sqlDataReader["StartTime"].ToString();
                        poiDefineInfo.weekdates = sqlDataReader["WeekDates"].ToString();
                        int num = (int)sqlDataReader["CarID"];
                        try
                        {
                            this.carpoiht.Add((object)(num.ToString() + "-" + (object)poiDefineInfo.poiid), (object)poiDefineInfo);
                        }
                        catch (Exception ex)
                        {
                            FileOpetation.SaveRecord(string.Format("Initialiaze Exception:" + ex.Message + "(" + ex.StackTrace.Substring(ex.StackTrace.IndexOf("line") + 4).Trim() + ")\r\n", (object)DateTime.Now));
                        }
                    }
                }
                FileOpetation.SaveRecord("Initialize POI alarm\r\n");
                FileOpetation.SaveRecord(string.Format("loading finished!", (object)DateTime.Now));
            }
            catch (Exception ex)
            {
                FileOpetation.SaveRecord(string.Format("Initialiaze Exception:" + ex.Message + "(" + ex.StackTrace.Substring(ex.StackTrace.IndexOf("line") + 4).Trim() + ")\r\n", (object)DateTime.Now));
            }
        }

        private string formatnum(int num)
        {
            string str = num.ToString();
            if (num < 10)
                str = "0" + str;
            return str;
        }

        private string formatnum(int num, int count)
        {
            string str = num.ToString();
            while (str.Length < count)
                str = "0" + str;
            return str;
        }

        private void CallbackTask(object stateInfo)
        {
            if (this.handleing)
                return;
            this.handleing = true;
            try
            {
                ++this.num;
                StringBuilder stringBuilder1 = new StringBuilder();
                StringBuilder stringBuilder2 = new StringBuilder();
                DateTime now = DateTime.Now;
                int num1 = 0;
                DataTable dt = new DataTable("Datas");
                dt.Columns.Add("ImeiNo", System.Type.GetType("System.String"));
                dt.Columns.Add("Latitude", System.Type.GetType("System.String"));
                dt.Columns.Add("Longitude", System.Type.GetType("System.String"));
                dt.Columns.Add("Speed", System.Type.GetType("System.Decimal"));
                dt.Columns.Add("Direct", System.Type.GetType("System.String"));
                dt.Columns.Add("Fuel", System.Type.GetType("System.Decimal"));
                dt.Columns.Add("Fuel2", System.Type.GetType("System.Decimal"));
                dt.Columns.Add("Mileage", System.Type.GetType("System.Decimal"));
                dt.Columns.Add("Temperature", System.Type.GetType("System.Decimal"));
                dt.Columns.Add("GPSTime", System.Type.GetType("System.DateTime"));
                dt.Columns.Add("CreateTime", System.Type.GetType("System.DateTime"));
                dt.Columns.Add("SquadDate", System.Type.GetType("System.DateTime"));
                dt.Columns.Add("Address", System.Type.GetType("System.String"));
                dt.Columns.Add("Input1", System.Type.GetType("System.Int32"));
                dt.Columns.Add("Input2", System.Type.GetType("System.Int32"));
                dt.Columns.Add("Input3", System.Type.GetType("System.Int32"));
                dt.Columns.Add("Input4", System.Type.GetType("System.Int32"));
                dt.Columns.Add("Input5", System.Type.GetType("System.Int32"));
                dt.Columns.Add("Output1", System.Type.GetType("System.Int32"));
                dt.Columns.Add("Output2", System.Type.GetType("System.Int32"));
                dt.Columns.Add("Output3", System.Type.GetType("System.Int32"));
                dt.Columns.Add("Output4", System.Type.GetType("System.Int32"));
                dt.Columns.Add("Output5", System.Type.GetType("System.Int32"));
                dt.Columns.Add("Engine", System.Type.GetType("System.Int32"));
                dt.Columns.Add("Working", System.Type.GetType("System.Int32"));
                dt.Columns.Add("AlarmText", System.Type.GetType("System.String"));
                dt.Columns.Add("StatusText", System.Type.GetType("System.String"));
                dt.Columns.Add("DriverID", System.Type.GetType("System.Int32"));
                dt.Columns.Add("DistrictID", System.Type.GetType("System.Int32"));
                dt.Columns.Add("BlockID", System.Type.GetType("System.Int32"));
                dt.Columns.Add("StackID", System.Type.GetType("System.Int32"));
                dt.Columns.Add("InGeofenceID", System.Type.GetType("System.Int32"));
                dt.Columns.Add("OutGeofenceID", System.Type.GetType("System.Int32"));
                dt.Columns.Add("TerminalID", System.Type.GetType("System.Int32"));
                dt.Columns.Add("StackAddress", System.Type.GetType("System.String"));
                dt.Columns.Add("GPS", System.Type.GetType("System.String"));
                dt.Columns.Add("Signal", System.Type.GetType("System.String"));
                dt.Columns.Add("POIID", System.Type.GetType("System.Int32"));
                FileOpetation.SaveRecord("checkmemory\r\n");
                using (DataTable table = SqlHelper.ExecuteDataset("select top 300 * from checkmemory order by id").Tables[0])
                {
                    foreach (DataRow row in (InternalDataCollectionBase)table.Rows)
                    {
                        stringBuilder1.Append(row["id"].ToString() + ",");
                        string str1 = row["TableName"].ToString();
                        string str2 = row["Action"].ToString();
                        string str3 = row["Param"].ToString();
                        if (str1.Equals("StopAlarm"))
                        {
                            if (str2.Equals("InitParam"))
                            {
                                string[] strArray = str3.Split(',');
                                int num2 = int.Parse(strArray[0]);
                                if (this.statusht.ContainsKey((object)num2))
                                {
                                    StatusInfo statusInfo = (StatusInfo)this.statusht[(object)num2];
                                    int num3;
                                    if (this.caridht.ContainsKey((object)num2) && Utils.tracklistht.ContainsKey((object)this.caridht[(object)num2].ToString()) && (TrackListInfo)Utils.tracklistht[(object)this.caridht[(object)num2].ToString()] != null)
                                    {
                                        DateTime gpstime = ((TrackListInfo)Utils.tracklistht[(object)this.caridht[(object)num2].ToString()]).gpstime;
                                        num3 = false ? 1 : 0;
                                    }
                                    else
                                        num3 = 1;
                                    statusInfo.lastmovetime = num3 != 0 ? DateTime.Now : ((TrackListInfo)Utils.tracklistht[(object)this.caridht[(object)num2].ToString()]).gpstime;
                                    statusInfo.lastmovealarmtime = DateTime.MaxValue;
                                }
                                if (this.alarmdefineht.ContainsKey((object)num2))
                                {
                                    AlarmDefineInfo alarmDefineInfo = (AlarmDefineInfo)this.alarmdefineht[(object)num2];
                                    alarmDefineInfo.moveminute = double.Parse(strArray[1]);
                                    alarmDefineInfo.movespeed = double.Parse(strArray[2]);
                                    alarmDefineInfo.moveinterval = double.Parse(strArray[3]);
                                }
                            }
                        }
                        else if (str1.Equals("Car") && str2.Equals("State"))
                        {
                            CarInfo carInfo = (CarInfo)this.carht[(object)str3];
                            if (carInfo != null)
                            {
                                using (SqlDataReader sqlDataReader = SqlHelper.ExecuteReader("select * from car where imeino='" + str3 + "'"))
                                {
                                    if (sqlDataReader.Read())
                                        carInfo.state = (int)sqlDataReader["State"];
                                }
                            }
                        }
                    }
                }


                if (!stringBuilder1.ToString().Equals(""))
                    SqlHelper.ExecuteNonQuery("delete from checkmemory where id in (" + stringBuilder1.ToString().TrimEnd(',') + ")");


                object obj = SqlHelper.ExecuteScalar("select max(ID) from track with(nolock)");
                if (obj == DBNull.Value)
                    obj = (object)"0";

                using (DataTable table = SqlHelper.ExecuteDataset("select * from track with(nolock) where id<=" + obj.ToString()).Tables[0])
                {
                    FileOpetation.SaveRecord("track finish.\r\n");
                    foreach (DataRow row1 in (InternalDataCollectionBase)table.Rows)
                    {
                        try
                        {
                            ++num1;
                            string s1 = row1["latitude"].ToString();
                            string s2 = row1["longitude"].ToString();
                            if (this.carht.ContainsKey((object)row1["ImeiNo"].ToString().Trim()) && row1["imeino"].ToString().Length > 2 && double.Parse(s1) != 0.0 && double.Parse(s2) != 0.0)
                            {
                                CarInfo carInfo = (CarInfo)this.carht[(object)row1["ImeiNo"].ToString().Trim()];
                                int carid = carInfo.carid;
                                int companyid = carInfo.companyid;
                                int protocol = carInfo.protocol;
                                double timezone = carInfo.timezone;
                                string str1 = "North";
                                double num2 = (double)row1["Direct"];
                                if (num2 <= 22.5 || num2 > 337.5)
                                    str1 = "North";
                                else if (num2 > 22.5 && num2 <= 67.5)
                                    str1 = "Northeast";
                                else if (num2 > 67.5 && num2 <= 112.5)
                                    str1 = "East";
                                else if (num2 > 112.5 && num2 <= 157.5)
                                    str1 = "Southeast";
                                else if (num2 > 157.5 && num2 <= 202.5)
                                    str1 = "South";
                                else if (num2 > 202.5 && num2 <= 247.5)
                                    str1 = "Southwest";
                                else if (num2 > 247.5 && num2 <= 292.5)
                                    str1 = "West";
                                else if (num2 > 292.5 && num2 <= 337.5)
                                    str1 = "Northwest";
                                string xml = "";
                                Exception exception;
                                if (!carInfo.mapinfo.Equals(""))
                                {
                                    bool flag = true;
                                    if (this.addressht.ContainsKey((object)carid))
                                    {
                                        string[] strArray = this.addressht[(object)carid].ToString().Split('|');
                                        if (Utils.GetDistance(double.Parse(strArray[0]), double.Parse(strArray[1]), double.Parse(s1), double.Parse(s2)) * 1000.0 < 5000.0)
                                        {
                                            flag = false;
                                            xml = strArray[2];
                                        }
                                    }
                                    if (flag)
                                    {
                                        if (carInfo.mapinfo.Equals("bi"))
                                        {
                                            try
                                            {
                                                xml = Utils.HttpGet("http://nominatim.openstreetmap.org/reverse?format=xml&lat=" + s1 + "&lon=" + s2 + "&zoom=18&addressdetails=1");
                                                XmlDocument xmlDocument = new XmlDocument();
                                                xmlDocument.LoadXml(xml);
                                                xml = xmlDocument.SelectSingleNode("reversegeocode/result").InnerText;
                                                this.addressht.Remove((object)carid);
                                                this.addressht.Add((object)carid, (object)(s1 + "|" + s2 + "|" + xml));
                                            }
                                            catch (Exception ex)
                                            {
                                                exception = ex;
                                            }
                                        }
                                        else
                                        {
                                            xml = Utils.HttpGet("http://localhost:8080/MapInfo/FeatureServlet?map=" + carInfo.mapinfo + "&latitude=" + s1 + "&longitude=" + s2);
                                            this.addressht.Remove((object)carid);
                                            this.addressht.Add((object)carid, (object)(s1 + "|" + s2 + "|" + xml));
                                        }
                                    }
                                }
                                int num3 = 0;
                                int num4 = 0;
                                int num5 = 0;
                                int num6 = 0;
                                int num7 = 0;
                                int num8 = 0;
                                int num9 = 0;
                                int num10 = 0;
                                int num11 = 0;
                                int num12 = 0;
                                if (row1["Protocol"].Equals((object)"TK310"))
                                {
                                    string str2 = "0000000000000000";
                                    try
                                    {
                                        str2 = Convert.ToString(Convert.ToInt32(row1["StatusBit"].ToString(), 16), 2);
                                    }
                                    catch (Exception ex)
                                    {
                                        exception = ex;
                                    }
                                    while (str2.Length < 16)
                                        str2 = "0" + str2;
                                    num3 = int.Parse(str2.Substring(15, 1));
                                    num4 = int.Parse(str2.Substring(14, 1));
                                    num5 = int.Parse(str2.Substring(13, 1));
                                    num6 = int.Parse(str2.Substring(12, 1));
                                    num7 = int.Parse(str2.Substring(11, 1));
                                    num8 = int.Parse(str2.Substring(7, 1));
                                    num9 = int.Parse(str2.Substring(6, 1));
                                    num10 = int.Parse(str2.Substring(5, 1));
                                    num11 = int.Parse(str2.Substring(4, 1));
                                    num12 = int.Parse(str2.Substring(3, 1));
                                }
                                string str3 = row1["AlarmText"].ToString();
                                int num13 = 0;
                                int num14 = 0;
                                string str4 = "";
                                bool flag1;
                                if (row1["Protocol"].Equals((object)"TK310") && (int)row1["AlarmBit"] != 0)
                                {
                                    int num15 = (int)row1["AlarmBit"];
                                    flag1 = false;
                                    if (num15 == 1)
                                        str3 += "SOS Alarm; ";
                                    if (num15 == 16)
                                        str3 += "Low Battery Alarm;";
                                    else if (num15 == 17)
                                        str3 += "Over Speed Alarm;";
                                    else if (num15 == 18)
                                        str3 += "Movement Alarm;";
                                    else if (num15 == 19)
                                        str3 += "Geo-Fence Alarm;";
                                    else if (num15 == 20)
                                        str3 += "Impact Alarm;";
                                    else if (num15 == 21)
                                        str3 += "Enter Blind Alarm;";
                                    else if (num15 == 22)
                                        str3 += "Exit Blind Alarm;";
                                    else if (num15 == 25)
                                        str3 += "Shake Alarm;";
                                    else if (num15 == 68)
                                        str3 += "No DLC Detected;";
                                    else if (num15 == 69)
                                        str3 += "Not Allowed DLC;";
                                    else if (num15 == 80)
                                        str3 += "Cut External Power Alarm;";
                                    else if (num15 == 82)
                                        str3 += "Veer Report Alarm;";
                                    else if (num15 == 113)
                                        str3 += "Veer Report Alarm;";
                                    else if (num15 == 114)
                                        str3 += "Harsh Braking Alarm;";
                                    else if (num15 == 115)
                                        str3 += "Harsh Acceleration Alarm;";
                                    else if (num15 == 116)
                                        str3 += "Fuel Steal Alarm;";
                                    else if (num15 == 117)
                                        str3 += "Camera Error Alarm;";
                                    else if (num15 == 119)
                                        str3 += "Alcohol Alarm;";
                                    else if (num15 == 120)
                                        str3 += "High Temp Alarm;";
                                    else if (num15 == 121)
                                        str3 += "Low Temp Alarm;";
                                }
                                try
                                {
                                    if (this.fencedefineht.ContainsKey((object)carid))
                                    {
                                        ArrayList arrayList = (ArrayList)this.fencedefineht[(object)carid];
                                        TrackListInfo trackListInfo = (TrackListInfo)null;
                                        if (Utils.tracklistht.ContainsKey((object)row1["imeino"].ToString()))
                                            trackListInfo = (TrackListInfo)Utils.tracklistht[(object)row1["imeino"].ToString()];
                                        for (int index = 0; index < arrayList.Count; ++index)
                                        {
                                            FenceDefineInfo fenceDefineInfo = (FenceDefineInfo)arrayList[index];
                                            DateTime dateTime = DateTime.Parse(row1["GPSTime"].ToString());
                                            string str2 = dateTime.ToString("yyyy-MM-dd");
                                            string str5 = dateTime.ToString("HH:mm:ss");
                                            if (this.tracklistfenceht.ContainsKey((object)fenceDefineInfo.carfenceid))
                                            {
                                                bool flag2 = false;
                                                switch (fenceDefineInfo.geofencekindid)
                                                {
                                                    case 1:
                                                        string[] strArray = fenceDefineInfo.latlngs.Split('|');
                                                        double num15 = Math.Min(double.Parse(strArray[0].Split(',')[0]), double.Parse(strArray[1].Split(',')[0]));
                                                        double num16 = Math.Max(double.Parse(strArray[0].Split(',')[0]), double.Parse(strArray[1].Split(',')[0]));
                                                        double num17 = Math.Min(double.Parse(strArray[0].Split(',')[1]), double.Parse(strArray[1].Split(',')[1]));
                                                        double num18 = Math.Max(double.Parse(strArray[0].Split(',')[1]), double.Parse(strArray[1].Split(',')[1]));
                                                        if (fenceDefineInfo.inorout == 1 && double.Parse(s1) < num16 && (double.Parse(s1) > num15 && double.Parse(s2) < num18) && double.Parse(s2) > num17 && (trackListInfo == null || (double.Parse(trackListInfo.latitude) > num16 || double.Parse(trackListInfo.latitude) < num15 || double.Parse(trackListInfo.longitude) > num18 || double.Parse(trackListInfo.longitude) < num17)))
                                                        {
                                                            flag2 = true;
                                                            break;
                                                        }
                                                        if (fenceDefineInfo.inorout == 0 && (double.Parse(s1) > num16 || double.Parse(s1) < num15 || (double.Parse(s2) > num18 || double.Parse(s2) < num17)) && (trackListInfo == null || double.Parse(trackListInfo.latitude) < num16 && double.Parse(trackListInfo.latitude) > num15 && double.Parse(trackListInfo.longitude) < num18 && double.Parse(trackListInfo.longitude) > num17))
                                                        {
                                                            flag2 = true;
                                                            break;
                                                        }
                                                        break;
                                                    case 2:
                                                        double lat1 = double.Parse(fenceDefineInfo.latlngs.Split(',')[0]);
                                                        double lng1 = double.Parse(fenceDefineInfo.latlngs.Split(',')[1]);
                                                        double distance = Utils.GetDistance(lat1, lng1, double.Parse(s1), double.Parse(s2));
                                                        double num19 = 0.0;
                                                        if (trackListInfo != null)
                                                            num19 = Utils.GetDistance(lat1, lng1, double.Parse(trackListInfo.latitude), double.Parse(trackListInfo.longitude));
                                                        if (fenceDefineInfo.inorout == 1 && distance < fenceDefineInfo.radius && (trackListInfo == null || num19 > fenceDefineInfo.radius))
                                                        {
                                                            flag2 = true;
                                                            break;
                                                        }
                                                        if (fenceDefineInfo.inorout == 0 && distance > fenceDefineInfo.radius && (trackListInfo == null || num19 < fenceDefineInfo.radius))
                                                        {
                                                            flag2 = true;
                                                            break;
                                                        }
                                                        break;
                                                    case 3:
                                                        string[] ps = fenceDefineInfo.latlngs.Split('|');
                                                        if (fenceDefineInfo.inorout == 1 && Utils.isPointInPolygon(ps, double.Parse(s2), double.Parse(s1)) && (trackListInfo == null || !Utils.isPointInPolygon(ps, double.Parse(trackListInfo.longitude), double.Parse(trackListInfo.latitude))))
                                                            flag2 = true;
                                                        else if (fenceDefineInfo.inorout == 0 && !Utils.isPointInPolygon(ps, double.Parse(s2), double.Parse(s1)) && (trackListInfo == null || Utils.isPointInPolygon(ps, double.Parse(trackListInfo.longitude), double.Parse(trackListInfo.latitude))))
                                                            flag2 = true;
                                                        break;
                                                }
                                                if (flag2)
                                                {
                                                    TrackListFenceInfo trackListFenceInfo = (TrackListFenceInfo)this.tracklistfenceht[(object)fenceDefineInfo.carfenceid];
                                                    trackListFenceInfo.endlatitude = row1["Latitude"].ToString();
                                                    trackListFenceInfo.endlongitude = row1["Longitude"].ToString();
                                                    trackListFenceInfo.endmileage = double.Parse(row1["Mileage"].ToString());
                                                    trackListFenceInfo.endtime = DateTime.Parse(row1["GPSTime"].ToString());
                                                    TimeSpan timeSpan = trackListFenceInfo.endtime.Subtract(trackListFenceInfo.starttime);
                                                    trackListFenceInfo.duration = Utils.FormatDuration(timeSpan.TotalSeconds);
                                                    trackListFenceInfo.mileage = (trackListFenceInfo.endmileage - trackListFenceInfo.startmileage).ToString();
                                                    StringBuilder stringBuilder3 = new StringBuilder();
                                                    stringBuilder3.Append("insert into tracklistfence(");
                                                    stringBuilder3.Append("ImeiNo,SquadDate,CarFenceID,GeoFenceID,StartLatitude,StartLongitude,StartTime,StartMileage,EndLatitude,EndLongitude,EndTime,EndMileage,Duration,Mileage)");
                                                    stringBuilder3.Append(" values (");
                                                    stringBuilder3.Append("@ImeiNo,@SquadDate,@CarFenceID,@GeoFenceID,@StartLatitude,@StartLongitude,@StartTime,@StartMileage,@EndLatitude,@EndLongitude,@EndTime,@EndMileage,@Duration,@Mileage)");
                                                    SqlParameter[] sqlParameterArray = new SqlParameter[14]
                                                    {
                            new SqlParameter("@ImeiNo", SqlDbType.VarChar, 50),
                            new SqlParameter("@SquadDate", SqlDbType.DateTime),
                            new SqlParameter("@CarFenceID", SqlDbType.Int, 4),
                            new SqlParameter("@GeoFenceID", SqlDbType.Int, 4),
                            new SqlParameter("@StartLatitude", SqlDbType.VarChar, 50),
                            new SqlParameter("@StartLongitude", SqlDbType.VarChar, 50),
                            new SqlParameter("@StartTime", SqlDbType.DateTime),
                            new SqlParameter("@StartMileage", SqlDbType.Float),
                            new SqlParameter("@EndLatitude", SqlDbType.VarChar, 50),
                            new SqlParameter("@EndLongitude", SqlDbType.VarChar, 50),
                            new SqlParameter("@EndTime", SqlDbType.DateTime),
                            new SqlParameter("@EndMileage", SqlDbType.Float),
                            new SqlParameter("@Duration", SqlDbType.VarChar, 50),
                            new SqlParameter("@Mileage", SqlDbType.VarChar, 50)
                                                    };
                                                    sqlParameterArray[0].Value = (object)trackListFenceInfo.imeino;
                                                    sqlParameterArray[1].Value = (object)trackListFenceInfo.squaddate;
                                                    sqlParameterArray[2].Value = (object)trackListFenceInfo.carfenceid;
                                                    sqlParameterArray[3].Value = (object)trackListFenceInfo.geofenceid;
                                                    sqlParameterArray[4].Value = (object)trackListFenceInfo.startlatitude;
                                                    sqlParameterArray[5].Value = (object)trackListFenceInfo.startlongitude;
                                                    sqlParameterArray[6].Value = (object)trackListFenceInfo.starttime;
                                                    sqlParameterArray[7].Value = (object)trackListFenceInfo.startmileage;
                                                    sqlParameterArray[8].Value = (object)trackListFenceInfo.endlatitude;
                                                    sqlParameterArray[9].Value = (object)trackListFenceInfo.endlongitude;
                                                    sqlParameterArray[10].Value = (object)trackListFenceInfo.endtime;
                                                    sqlParameterArray[11].Value = (object)trackListFenceInfo.endmileage;
                                                    sqlParameterArray[12].Value = (object)trackListFenceInfo.duration;
                                                    sqlParameterArray[13].Value = (object)trackListFenceInfo.mileage;
                                                    SqlHelper.ExecuteNonQuery(stringBuilder3.ToString(), sqlParameterArray);
                                                    this.tracklistfenceht.Remove((object)fenceDefineInfo.carfenceid);
                                                }
                                            }
                                            else
                                            {
                                                bool flag2 = false;
                                                bool flag3 = false;
                                                if ((fenceDefineInfo.always == 1 || (fenceDefineInfo.weekdates.IndexOf(Utils.GetWeekDay(DateTime.Parse(row1["GPSTime"].ToString())).ToString()) >= 0 || fenceDefineInfo.fencedate.Equals(str2))) && str5.CompareTo(fenceDefineInfo.starttime) >= 0 && str5.CompareTo(fenceDefineInfo.endtime) <= 0)
                                                {
                                                    switch (fenceDefineInfo.geofencekindid)
                                                    {
                                                        case 1:
                                                            string[] strArray = fenceDefineInfo.latlngs.Split('|');
                                                            double num15 = Math.Min(double.Parse(strArray[0].Split(',')[0]), double.Parse(strArray[1].Split(',')[0]));
                                                            double num16 = Math.Max(double.Parse(strArray[0].Split(',')[0]), double.Parse(strArray[1].Split(',')[0]));
                                                            double num17 = Math.Min(double.Parse(strArray[0].Split(',')[1]), double.Parse(strArray[1].Split(',')[1]));
                                                            double num18 = Math.Max(double.Parse(strArray[0].Split(',')[1]), double.Parse(strArray[1].Split(',')[1]));
                                                            if (str3.IndexOf("In Fence Alarm") < 0 && fenceDefineInfo.inorout == 0 && (double.Parse(s1) < num16 && double.Parse(s1) > num15) && (double.Parse(s2) < num18 && double.Parse(s2) > num17) && (trackListInfo == null || (double.Parse(trackListInfo.latitude) > num16 || double.Parse(trackListInfo.latitude) < num15 || double.Parse(trackListInfo.longitude) > num18 || double.Parse(trackListInfo.longitude) < num17)))
                                                            {
                                                                str3 += "In Fence Alarm; ";
                                                                num13 = fenceDefineInfo.geofenceid;
                                                                str4 = fenceDefineInfo.fencename;
                                                                flag2 = true;
                                                                break;
                                                            }
                                                            if (str3.IndexOf("Out Fence Alarm") < 0 && fenceDefineInfo.inorout == 1 && (double.Parse(s1) > num16 || double.Parse(s1) < num15 || (double.Parse(s2) > num18 || double.Parse(s2) < num17)) && (trackListInfo == null || double.Parse(trackListInfo.latitude) < num16 && double.Parse(trackListInfo.latitude) > num15 && double.Parse(trackListInfo.longitude) < num18 && double.Parse(trackListInfo.longitude) > num17))
                                                            {
                                                                str3 += "Out Fence Alarm; ";
                                                                num14 = fenceDefineInfo.geofenceid;
                                                                str4 = fenceDefineInfo.fencename;
                                                                flag3 = true;
                                                                break;
                                                            }
                                                            break;
                                                        case 2:
                                                            double lat1 = double.Parse(fenceDefineInfo.latlngs.Split(',')[0]);
                                                            double lng1 = double.Parse(fenceDefineInfo.latlngs.Split(',')[1]);
                                                            double distance = Utils.GetDistance(lat1, lng1, double.Parse(s1), double.Parse(s2));
                                                            double num19 = 0.0;
                                                            if (trackListInfo != null)
                                                                num19 = Utils.GetDistance(lat1, lng1, double.Parse(trackListInfo.latitude), double.Parse(trackListInfo.longitude));
                                                            if (str3.IndexOf("In Fence Alarm") < 0 && fenceDefineInfo.inorout == 0 && distance < fenceDefineInfo.radius && (trackListInfo == null || num19 > fenceDefineInfo.radius))
                                                            {
                                                                str3 += "In Fence Alarm; ";
                                                                num13 = fenceDefineInfo.geofenceid;
                                                                str4 = fenceDefineInfo.fencename;
                                                                flag2 = true;
                                                                break;
                                                            }
                                                            if (str3.IndexOf("Out Fence Alarm") < 0 && fenceDefineInfo.inorout == 1 && distance > fenceDefineInfo.radius && (trackListInfo == null || num19 < fenceDefineInfo.radius))
                                                            {
                                                                str3 += "Out Fence Alarm; ";
                                                                num14 = fenceDefineInfo.geofenceid;
                                                                str4 = fenceDefineInfo.fencename;
                                                                flag3 = true;
                                                                break;
                                                            }
                                                            break;
                                                        case 3:
                                                            string[] ps = fenceDefineInfo.latlngs.Split('|');
                                                            if (str3.IndexOf("In Fence Alarm") < 0 && fenceDefineInfo.inorout == 0 && Utils.isPointInPolygon(ps, double.Parse(s2), double.Parse(s1)) && (trackListInfo == null || !Utils.isPointInPolygon(ps, double.Parse(trackListInfo.longitude), double.Parse(trackListInfo.latitude))))
                                                            {
                                                                str3 += "In Fence Alarm; ";
                                                                num13 = fenceDefineInfo.geofenceid;
                                                                str4 = fenceDefineInfo.fencename;
                                                                flag2 = true;
                                                            }
                                                            else if (str3.IndexOf("Out Fence Alarm") < 0 && fenceDefineInfo.inorout == 1 && !Utils.isPointInPolygon(ps, double.Parse(s2), double.Parse(s1)) && (trackListInfo == null || Utils.isPointInPolygon(ps, double.Parse(trackListInfo.longitude), double.Parse(trackListInfo.latitude))))
                                                            {
                                                                str3 += "Out Fence Alarm; ";
                                                                num14 = fenceDefineInfo.geofenceid;
                                                                str4 = fenceDefineInfo.fencename;
                                                                flag3 = true;
                                                            }
                                                            break;
                                                    }
                                                }
                                                if (flag2 || flag3)
                                                {
                                                    this.tracklistfenceht.Add((object)fenceDefineInfo.carfenceid, (object)new TrackListFenceInfo()
                                                    {
                                                        imeino = row1["ImeiNO"].ToString(),
                                                        squaddate = DateTime.Parse(dateTime.ToString("yyyy-MM-dd")),
                                                        carfenceid = fenceDefineInfo.carfenceid,
                                                        geofenceid = fenceDefineInfo.geofenceid,
                                                        startlatitude = row1["Latitude"].ToString(),
                                                        startlongitude = row1["Longitude"].ToString(),
                                                        startmileage = double.Parse(row1["Mileage"].ToString()),
                                                        starttime = DateTime.Parse(row1["GPSTime"].ToString()),
                                                        duration = "",
                                                        mileage = ""
                                                    });
                                                    try
                                                    {
                                                        if (fenceDefineInfo.cutoil == 1)
                                                        {
                                                            if (carInfo.protocol == 10)
                                                            {
                                                                StringBuilder stringBuilder3 = new StringBuilder();
                                                                stringBuilder3.Append("insert into commandlist(");
                                                                stringBuilder3.Append("carid,commandid,params,createtime,creatorid)");
                                                                stringBuilder3.Append(" values (");
                                                                stringBuilder3.Append("@carid,@commandid,@params,@createtime,@creatorid)");
                                                                stringBuilder3.Append(";select SCOPE_IDENTITY()");
                                                                SqlParameter[] sqlParameterArray = new SqlParameter[5]
                                                                {
                                  new SqlParameter("@carid", SqlDbType.Int, 4),
                                  new SqlParameter("@commandid", SqlDbType.Int, 4),
                                  new SqlParameter("@params", SqlDbType.NVarChar, 500),
                                  new SqlParameter("@createtime", SqlDbType.DateTime),
                                  new SqlParameter("@creatorid", SqlDbType.Int, 4)
                                                                };
                                                                sqlParameterArray[0].Value = (object)carid;
                                                                sqlParameterArray[1].Value = (object)37;
                                                                sqlParameterArray[2].Value = (object)"";
                                                                sqlParameterArray[3].Value = (object)DateTime.Now;
                                                                sqlParameterArray[4].Value = (object)0;
                                                                SqlHelper.ExecuteNonQuery(stringBuilder3.ToString(), sqlParameterArray);
                                                            }
                                                            else
                                                            {
                                                                StringBuilder stringBuilder3 = new StringBuilder();
                                                                stringBuilder3.Append("insert into commandlist(");
                                                                stringBuilder3.Append("carid,commandid,params,createtime,creatorid)");
                                                                stringBuilder3.Append(" values (");
                                                                stringBuilder3.Append("@carid,@commandid,@params,@createtime,@creatorid)");
                                                                stringBuilder3.Append(";select SCOPE_IDENTITY()");
                                                                SqlParameter[] sqlParameterArray = new SqlParameter[5]
                                                                {
                                  new SqlParameter("@carid", SqlDbType.Int, 4),
                                  new SqlParameter("@commandid", SqlDbType.Int, 4),
                                  new SqlParameter("@params", SqlDbType.NVarChar, 500),
                                  new SqlParameter("@createtime", SqlDbType.DateTime),
                                  new SqlParameter("@creatorid", SqlDbType.Int, 4)
                                                                };
                                                                sqlParameterArray[0].Value = (object)carid;
                                                                sqlParameterArray[1].Value = (object)15;
                                                                sqlParameterArray[2].Value = (object)"1,2,2,2,2";
                                                                sqlParameterArray[3].Value = (object)DateTime.Now;
                                                                sqlParameterArray[4].Value = (object)0;
                                                                SqlHelper.ExecuteNonQuery(stringBuilder3.ToString(), sqlParameterArray);
                                                            }
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        FileOpetation.SaveRecord("Cut Oil Error:" + ex.Message + "\r\n");
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    FileOpetation.SaveRecord(carInfo.imeino + ":" + ex.Message + "\r\n");
                                }
                                int num20 = 0;
                                if (this.fencespeeddefineht.ContainsKey((object)carid))
                                {
                                    ArrayList arrayList = (ArrayList)this.fencespeeddefineht[(object)carid];
                                    for (int index = 0; index < arrayList.Count; ++index)
                                    {
                                        FenceSpeedDefineInfo fenceSpeedDefineInfo = (FenceSpeedDefineInfo)arrayList[index];
                                        string str2 = row1["GPSTime"].ToString().Split(' ')[0];
                                        string str5 = row1["GPSTime"].ToString().Split(' ')[1];
                                        if ((fenceSpeedDefineInfo.datetype == 0 || (fenceSpeedDefineInfo.weekdates.IndexOf(Utils.GetWeekDay(DateTime.Parse(row1["GPSTime"].ToString())).ToString()) >= 0 || fenceSpeedDefineInfo.areadate.Equals(str2))) && str5.CompareTo(fenceSpeedDefineInfo.starttime) >= 0 && str5.CompareTo(fenceSpeedDefineInfo.endtime) <= 0 && (fenceSpeedDefineInfo.speed > 0.0 && (double)row1["speed"] > fenceSpeedDefineInfo.speed))
                                        {
                                            switch (fenceSpeedDefineInfo.geofencekindid)
                                            {
                                                case 1:
                                                    string[] strArray = fenceSpeedDefineInfo.latlngs.Split('|');
                                                    double num15 = Math.Min(double.Parse(strArray[0].Split(',')[0]), double.Parse(strArray[1].Split(',')[0]));
                                                    double num16 = Math.Max(double.Parse(strArray[0].Split(',')[0]), double.Parse(strArray[1].Split(',')[0]));
                                                    double num17 = Math.Min(double.Parse(strArray[0].Split(',')[1]), double.Parse(strArray[1].Split(',')[1]));
                                                    double num18 = Math.Max(double.Parse(strArray[0].Split(',')[1]), double.Parse(strArray[1].Split(',')[1]));
                                                    if (double.Parse(s1) < num16 && double.Parse(s1) > num15 && double.Parse(s2) < num18 && double.Parse(s2) > num17)
                                                    {
                                                        str3 += "Area Speed Alarm; ";
                                                        num20 = fenceSpeedDefineInfo.geofenceid;
                                                        break;
                                                    }
                                                    break;
                                                case 2:
                                                    if (Utils.GetDistance(double.Parse(fenceSpeedDefineInfo.latlngs.Split(',')[0]), double.Parse(fenceSpeedDefineInfo.latlngs.Split(',')[1]), double.Parse(s1), double.Parse(s2)) < fenceSpeedDefineInfo.radius)
                                                    {
                                                        str3 += "Area Speed Alarm; ";
                                                        num20 = fenceSpeedDefineInfo.geofenceid;
                                                        break;
                                                    }
                                                    break;
                                                case 3:
                                                    if (Utils.isPointInPolygon(fenceSpeedDefineInfo.latlngs.Split('|'), double.Parse(s2), double.Parse(s1)))
                                                    {
                                                        str3 += "Area Speed Alarm; ";
                                                        num20 = fenceSpeedDefineInfo.geofenceid;
                                                    }
                                                    break;
                                            }
                                        }
                                    }
                                }
                                if (this.fencecutoildefineht.ContainsKey((object)carid))
                                {
                                    bool flag2 = false;
                                    ArrayList arrayList = (ArrayList)this.fencecutoildefineht[(object)carid];
                                    TrackListInfo trackListInfo = (TrackListInfo)null;
                                    if (Utils.tracklistht.ContainsKey((object)row1["imeino"].ToString()))
                                        trackListInfo = (TrackListInfo)Utils.tracklistht[(object)row1["imeino"].ToString()];
                                    for (int index = 0; index < arrayList.Count; ++index)
                                    {
                                        FenceCutOilDefineInfo cutOilDefineInfo = (FenceCutOilDefineInfo)arrayList[index];
                                        DateTime dateTime = DateTime.Parse(row1["GPSTime"].ToString());
                                        string str2 = dateTime.ToString("yyyy-MM-dd");
                                        string str5 = dateTime.ToString("HH:mm:ss");
                                        FileOpetation.SaveRecord(cutOilDefineInfo.fencename + "|" + (object)cutOilDefineInfo.datetype + "|" + (object)cutOilDefineInfo.geofencekindid + "\r\n");
                                        if ((cutOilDefineInfo.datetype == 0 || (cutOilDefineInfo.weekdates.IndexOf(Utils.GetWeekDay(DateTime.Parse(row1["GPSTime"].ToString())).ToString()) >= 0 || cutOilDefineInfo.areadate.Equals(str2))) && str5.CompareTo(cutOilDefineInfo.starttime) >= 0 && str5.CompareTo(cutOilDefineInfo.endtime) <= 0)
                                        {
                                            int geofencekindid = cutOilDefineInfo.geofencekindid;
                                            switch (geofencekindid)
                                            {
                                                case 1:
                                                    string[] strArray = cutOilDefineInfo.latlngs.Split('|');
                                                    double num15 = Math.Min(double.Parse(strArray[0].Split(',')[0]), double.Parse(strArray[1].Split(',')[0]));
                                                    double num16 = Math.Max(double.Parse(strArray[0].Split(',')[0]), double.Parse(strArray[1].Split(',')[0]));
                                                    double num17 = Math.Min(double.Parse(strArray[0].Split(',')[1]), double.Parse(strArray[1].Split(',')[1]));
                                                    double num18 = Math.Max(double.Parse(strArray[0].Split(',')[1]), double.Parse(strArray[1].Split(',')[1]));
                                                    if (cutOilDefineInfo.inorout == 0 && double.Parse(s1) < num16 && (double.Parse(s1) > num15 && double.Parse(s2) < num18) && double.Parse(s2) > num17 && (trackListInfo == null || (double.Parse(trackListInfo.latitude) > num16 || double.Parse(trackListInfo.latitude) < num15 || double.Parse(trackListInfo.longitude) > num18 || double.Parse(trackListInfo.longitude) < num17)))
                                                    {
                                                        flag2 = true;
                                                        goto label_212;
                                                    }
                                                    else if (cutOilDefineInfo.inorout == 1 && (double.Parse(s1) > num16 || double.Parse(s1) < num15 || (double.Parse(s2) > num18 || double.Parse(s2) < num17)) && (trackListInfo == null || double.Parse(trackListInfo.latitude) < num16 && double.Parse(trackListInfo.latitude) > num15 && double.Parse(trackListInfo.longitude) < num18 && double.Parse(trackListInfo.longitude) > num17))
                                                    {
                                                        flag2 = true;
                                                        goto label_212;
                                                    }
                                                    else
                                                        break;
                                                case 2:
                                                    double lat1 = double.Parse(cutOilDefineInfo.latlngs.Split(',')[0]);
                                                    double lng1 = double.Parse(cutOilDefineInfo.latlngs.Split(',')[1]);
                                                    double distance = Utils.GetDistance(lat1, lng1, double.Parse(s1), double.Parse(s2));
                                                    double num19 = 0.0;
                                                    if (trackListInfo != null)
                                                        num19 = Utils.GetDistance(lat1, lng1, double.Parse(trackListInfo.latitude), double.Parse(trackListInfo.longitude));
                                                    if (cutOilDefineInfo.inorout == 0 && distance < cutOilDefineInfo.radius && (trackListInfo == null || num19 > cutOilDefineInfo.radius))
                                                    {
                                                        flag2 = true;
                                                        goto label_212;
                                                    }
                                                    else if (cutOilDefineInfo.inorout == 1 && distance > cutOilDefineInfo.radius && (trackListInfo == null || num19 < cutOilDefineInfo.radius))
                                                    {
                                                        flag2 = true;
                                                        goto label_212;
                                                    }
                                                    else
                                                        break;
                                                case 3:
                                                    string[] ps = cutOilDefineInfo.latlngs.Split('|');
                                                    FileOpetation.SaveRecord("Latlngs:" + (object)geofencekindid + "|" + (object)cutOilDefineInfo.inorout + "|" + (object)ps.Length + "|" + (object)Utils.isPointInPolygon(ps, double.Parse(s2), double.Parse(s1)) + "|" + (object)(trackListInfo == null) + "\r\n");
                                                    if (cutOilDefineInfo.inorout == 0 && Utils.isPointInPolygon(ps, double.Parse(s2), double.Parse(s1)) && (trackListInfo == null || !Utils.isPointInPolygon(ps, double.Parse(trackListInfo.longitude), double.Parse(trackListInfo.latitude))))
                                                    {
                                                        flag2 = true;
                                                        goto label_212;
                                                    }
                                                    else if (cutOilDefineInfo.inorout == 1 && !Utils.isPointInPolygon(ps, double.Parse(s2), double.Parse(s1)) && (trackListInfo == null || Utils.isPointInPolygon(ps, double.Parse(trackListInfo.longitude), double.Parse(trackListInfo.latitude))))
                                                    {
                                                        flag2 = true;
                                                        goto label_212;
                                                    }
                                                    else
                                                        break;
                                            }
                                        }
                                    }
                                label_212:
                                    if (flag2)
                                    {
                                        FileOpetation.SaveRecord("Cut Oil Send.\r\n");
                                        if (carInfo.protocol == 10)
                                        {
                                            StringBuilder stringBuilder3 = new StringBuilder();
                                            stringBuilder3.Append("insert into commandlist(");
                                            stringBuilder3.Append("carid,commandid,params,createtime,creatorid)");
                                            stringBuilder3.Append(" values (");
                                            stringBuilder3.Append("@carid,@commandid,@params,@createtime,@creatorid)");
                                            stringBuilder3.Append(";select SCOPE_IDENTITY()");
                                            SqlParameter[] sqlParameterArray = new SqlParameter[5]
                                            {
                        new SqlParameter("@carid", SqlDbType.Int, 4),
                        new SqlParameter("@commandid", SqlDbType.Int, 4),
                        new SqlParameter("@params", SqlDbType.NVarChar, 500),
                        new SqlParameter("@createtime", SqlDbType.DateTime),
                        new SqlParameter("@creatorid", SqlDbType.Int, 4)
                                            };
                                            sqlParameterArray[0].Value = (object)carid;
                                            sqlParameterArray[1].Value = (object)37;
                                            sqlParameterArray[2].Value = (object)"";
                                            sqlParameterArray[3].Value = (object)DateTime.Now;
                                            sqlParameterArray[4].Value = (object)0;
                                            SqlHelper.ExecuteNonQuery(stringBuilder3.ToString(), sqlParameterArray);
                                        }
                                        else
                                        {
                                            StringBuilder stringBuilder3 = new StringBuilder();
                                            stringBuilder3.Append("insert into commandlist(");
                                            stringBuilder3.Append("carid,commandid,params,createtime,creatorid)");
                                            stringBuilder3.Append(" values (");
                                            stringBuilder3.Append("@carid,@commandid,@params,@createtime,@creatorid)");
                                            stringBuilder3.Append(";select SCOPE_IDENTITY()");
                                            SqlParameter[] sqlParameterArray = new SqlParameter[5]
                                            {
                        new SqlParameter("@carid", SqlDbType.Int, 4),
                        new SqlParameter("@commandid", SqlDbType.Int, 4),
                        new SqlParameter("@params", SqlDbType.NVarChar, 500),
                        new SqlParameter("@createtime", SqlDbType.DateTime),
                        new SqlParameter("@creatorid", SqlDbType.Int, 4)
                                            };
                                            sqlParameterArray[0].Value = (object)carid;
                                            sqlParameterArray[1].Value = (object)15;
                                            sqlParameterArray[2].Value = (object)"1,2,2,2,2";
                                            sqlParameterArray[3].Value = (object)DateTime.Now;
                                            sqlParameterArray[4].Value = (object)0;
                                            SqlHelper.ExecuteNonQuery(stringBuilder3.ToString(), sqlParameterArray);
                                        }
                                    }
                                }
                                int num21 = (int)row1["Engine"];
                                int num22 = 0;
                                if (this.alarmdefineht.ContainsKey((object)carid))
                                {
                                    AlarmDefineInfo alarmDefineInfo = (AlarmDefineInfo)this.alarmdefineht[(object)carid];
                                    int engine = alarmDefineInfo.engine;
                                    if (engine == 1 && num8 == 1 || engine == 2 && num9 == 1 || (engine == 3 && num10 == 1 || engine == 4 && num11 == 1) || engine == 5 && num12 == 1)
                                        num21 = 1;
                                    int working = alarmDefineInfo.working;
                                    if (working == 1 && num8 == 1 || working == 2 && num9 == 1 || (working == 3 && num10 == 1 || working == 4 && num11 == 1) || working == 5 && num12 == 1)
                                        num22 = 1;
                                    if (alarmDefineInfo.input1on != 0 && num8 == 1)
                                        str3 = str3 + alarmDefineInfo.sinput1on + ";";
                                    if (alarmDefineInfo.input2on != 0 && num9 == 1)
                                        str3 = str3 + alarmDefineInfo.sinput2on + ";";
                                    if (alarmDefineInfo.input3on != 0 && num10 == 1)
                                        str3 = str3 + alarmDefineInfo.sinput3on + ";";
                                    if (alarmDefineInfo.input4on != 0 && num11 == 1)
                                        str3 = str3 + alarmDefineInfo.sinput4on + ";";
                                    if (alarmDefineInfo.input5on != 0 && num12 == 1)
                                        str3 = str3 + alarmDefineInfo.sinput5on + ";";
                                    if (alarmDefineInfo.input1off != 0 && num8 == 0)
                                        str3 = str3 + alarmDefineInfo.sinput1off + ";";
                                    if (alarmDefineInfo.input2off != 0 && num9 == 0)
                                        str3 = str3 + alarmDefineInfo.sinput2off + ";";
                                    if (alarmDefineInfo.input3off != 0 && num10 == 0)
                                        str3 = str3 + alarmDefineInfo.sinput3off + ";";
                                    if (alarmDefineInfo.input4off != 0 && num11 == 0)
                                        str3 = str3 + alarmDefineInfo.sinput4off + ";";
                                    if (alarmDefineInfo.input5off != 0 && num12 == 0)
                                        str3 = str3 + alarmDefineInfo.sinput5off + ";";
                                    if (alarmDefineInfo.speed > 0.0 && (double)row1["speed"] > alarmDefineInfo.speed)
                                        str3 += "Over Speed Alarm;";
                                    if (alarmDefineInfo.engineon == 1 && Utils.tracklistht.ContainsKey((object)row1["imeino"].ToString()))
                                    {
                                        TrackListInfo trackListInfo = (TrackListInfo)Utils.tracklistht[(object)row1["imeino"].ToString()];
                                        int num15;
                                        if (trackListInfo != null)
                                        {
                                            DateTime dateTime = DateTime.Parse(row1["gpstime"].ToString());
                                            if (dateTime.CompareTo(DateTime.Now.AddDays(5.0)) < 0)
                                            {
                                                dateTime = DateTime.Parse(row1["gpstime"].ToString());
                                                num15 = dateTime.CompareTo(trackListInfo.gpstime) <= 0 ? 1 : 0;
                                                goto label_250;
                                            }
                                        }
                                        num15 = 1;
                                    label_250:
                                        if (num15 == 0 && (num21 == 1 && trackListInfo.engine == 0))
                                            str3 += "Engine On Alarm; ";
                                    }
                                }
                                string str6 = row1["StatusText"].ToString();
                                if (row1["Protocol"].Equals((object)"TK310") && this.statusdefineht.ContainsKey((object)carid))
                                {
                                    str6 = num21 != 1 ? str6 + "Engine Off; " : str6 + "Engine On; ";
                                    StatusDefineInfo statusDefineInfo = (StatusDefineInfo)this.statusdefineht[(object)carid];
                                    if (!statusDefineInfo.output1on.Equals("") && num3 == 1)
                                        str6 = str6 + statusDefineInfo.output1on + ";";
                                    if (!statusDefineInfo.output2on.Equals("") && num4 == 1)
                                        str6 = str6 + statusDefineInfo.output2on + ";";
                                    if (!statusDefineInfo.output3on.Equals("") && num5 == 1)
                                        str6 = str6 + statusDefineInfo.output3on + ";";
                                    if (!statusDefineInfo.output4on.Equals("") && num6 == 1)
                                        str6 = str6 + statusDefineInfo.output4on + ";";
                                    if (!statusDefineInfo.output5on.Equals("") && num7 == 1)
                                        str6 = str6 + statusDefineInfo.output5on + ";";
                                    if (!statusDefineInfo.output1off.Equals("") && num3 == 0)
                                        str6 = str6 + statusDefineInfo.output1off + ";";
                                    if (!statusDefineInfo.output2off.Equals("") && num4 == 0)
                                        str6 = str6 + statusDefineInfo.output2off + ";";
                                    if (!statusDefineInfo.output3off.Equals("") && num5 == 0)
                                        str6 = str6 + statusDefineInfo.output3off + ";";
                                    if (!statusDefineInfo.output4off.Equals("") && num6 == 0)
                                        str6 = str6 + statusDefineInfo.output4off + ";";
                                    if (!statusDefineInfo.output5off.Equals("") && num7 == 0)
                                        str6 = str6 + statusDefineInfo.output5off + ";";
                                    if (!statusDefineInfo.input1on.Equals("") && num8 == 1)
                                        str6 = str6 + statusDefineInfo.input1on + ";";
                                    if (!statusDefineInfo.input2on.Equals("") && num9 == 1)
                                        str6 = str6 + statusDefineInfo.input2on + ";";
                                    if (!statusDefineInfo.input3on.Equals("") && num10 == 1)
                                        str6 = str6 + statusDefineInfo.input3on + ";";
                                    if (!statusDefineInfo.input4on.Equals("") && num11 == 1)
                                        str6 = str6 + statusDefineInfo.input4on + ";";
                                    if (!statusDefineInfo.input5on.Equals("") && num12 == 1)
                                        str6 = str6 + statusDefineInfo.input5on + ";";
                                    if (!statusDefineInfo.input1off.Equals("") && num8 == 0)
                                        str6 = str6 + statusDefineInfo.input1off + ";";
                                    if (!statusDefineInfo.input2off.Equals("") && num9 == 0)
                                        str6 = str6 + statusDefineInfo.input2off + ";";
                                    if (!statusDefineInfo.input3off.Equals("") && num10 == 0)
                                        str6 = str6 + statusDefineInfo.input3off + ";";
                                    if (!statusDefineInfo.input4off.Equals("") && num11 == 0)
                                        str6 = str6 + statusDefineInfo.input4off + ";";
                                    if (!statusDefineInfo.input5off.Equals("") && num12 == 0)
                                        str6 = str6 + statusDefineInfo.input5off + ";";
                                }
                                else if (row1["Protocol"].Equals((object)"VT328"))
                                    str6 = num21 != 1 ? str6 + "Engine Off; " : str6 + "Engine On; ";
                                double num23 = 0.0;
                                double num24 = 0.0;
                                if (row1["Protocol"].Equals((object)"TK310"))
                                {
                                    double num15 = 0.0;
                                    double num16 = 0.0;
                                    try
                                    {
                                        num15 = (double)Convert.ToInt32(row1["AD1"].ToString(), 16);
                                        num16 = (double)Convert.ToInt32(row1["AD2"].ToString(), 16);
                                    }
                                    catch (Exception ex)
                                    {
                                        exception = ex;
                                    }
                                    if (this.fueldefineht.ContainsKey((object)carid))
                                    {
                                        FuelDefineInfo fuelDefineInfo = (FuelDefineInfo)this.fueldefineht[(object)carid];
                                        int quart1 = fuelDefineInfo.quart1;
                                        int maxfuel1 = fuelDefineInfo.maxfuel1;
                                        int minfuel1 = fuelDefineInfo.minfuel1;
                                        int quart2 = fuelDefineInfo.quart2;
                                        int maxfuel2 = fuelDefineInfo.maxfuel2;
                                        int minfuel2 = fuelDefineInfo.minfuel2;
                                        if (quart1 != 0 && maxfuel1 != 0)
                                            num15 = (num15 - (double)minfuel1) / (double)(maxfuel1 - minfuel1) * (double)quart1;
                                        if (quart2 != 0 && maxfuel2 != 0)
                                            num16 = (num16 - (double)minfuel2) / (double)(maxfuel2 - minfuel2) * (double)quart2;
                                        num23 = Math.Round(num15, 2);
                                        num24 = Math.Round(num16, 2);
                                    }
                                }
                                else
                                    num23 = (double)row1["Fuel"];
                                int num25 = 0;
                                if (!row1["rfid"].ToString().Equals("") && this.driverht.ContainsKey((object)row1["rfid"].ToString()))
                                    num25 = (int)this.driverht[(object)row1["rfid"].ToString()];
                                if (num25 == 0 && this.cardriverht.ContainsKey((object)carid))
                                    num25 = (int)this.cardriverht[(object)carid];
                                int num26 = 0;
                                int num27 = 0;
                                int num28 = 0;
                                int num29 = 0;
                                StringBuilder stringBuilder4 = new StringBuilder();
                                DateTime dateTime1 = DateTime.Parse(row1["gpstime"].ToString());
                                if (timezone != 0.0)
                                    dateTime1 = dateTime1.AddMinutes(timezone);
                                DateTime dateTime2 = DateTime.Parse(dateTime1.ToString("yyyy-MM-dd"));
                                double num30 = 0.0;
                                try
                                {
                                    if ((double)row1["speed"] >= 3.0)
                                        num30 = (double)row1["speed"];
                                }
                                catch (Exception ex)
                                {
                                    exception = ex;
                                }
                                if (num30 >= 3.0)
                                    SqlHelper.ExecuteNonQuery("update car set lastmovetime='" + DateTime.Parse(row1["gpstime"].ToString()).ToString("yyyy-MM-dd HH:mm:ss") + "' where id=" + (object)carid);
                                double num31 = carInfo.basemileage + (double)row1["mileage"];
                                string str7 = row1["GPS"].ToString();
                                string str8 = row1["Signal"].ToString();
                                SqlHelper.ExecuteNonQuery("update tracklist_heart set engine=" + (object)num21 + ",speed=" + (object)num30 + " where imeino='" + row1["ImeiNo"].ToString().Trim() + "'");
                                int num32 = 0;
                                string str9 = "";
                                try
                                {
                                    if (this.poiinfoht.ContainsKey((object)carInfo.companyid))
                                    {
                                        ArrayList arrayList = (ArrayList)this.poiinfoht[(object)carInfo.companyid];
                                        for (int index = 0; index < arrayList.Count; ++index)
                                        {
                                            POIInfo poiInfo = (POIInfo)arrayList[index];
                                            double num15 = Utils.GetDistance(poiInfo.latitude, poiInfo.longitude, double.Parse(s1), double.Parse(s2)) * 1000.0;
                                            if (!this.poiht.ContainsKey((object)(row1["ImeiNO"].ToString() + "-" + (object)poiInfo.id)))
                                            {
                                                if (num15 < this.mindistance)
                                                {
                                                    int num16 = 0;
                                                    int num17 = 0;
                                                    TrackListPOIInfo trackListPoiInfo = new TrackListPOIInfo();
                                                    trackListPoiInfo.imeino = row1["ImeiNO"].ToString();
                                                    trackListPoiInfo.squaddate = dateTime2;
                                                    trackListPoiInfo.roundrouteid = 0;
                                                    trackListPoiInfo.routeid = num16;
                                                    trackListPoiInfo.siteid = num17;
                                                    trackListPoiInfo.poiid = poiInfo.id;
                                                    trackListPoiInfo.enterlatitude = row1["Latitude"].ToString();
                                                    trackListPoiInfo.enterlongitude = row1["Longitude"].ToString();
                                                    trackListPoiInfo.entermileage = (double)row1["Mileage"];
                                                    trackListPoiInfo.entertime = dateTime1;
                                                    trackListPoiInfo.kind = "UnPlan";
                                                    StringBuilder stringBuilder3 = new StringBuilder();
                                                    stringBuilder3.Append("insert into tracklistpoi(");
                                                    stringBuilder3.Append("ImeiNo,SquadDate,RouteID,SiteID,POIID,EnterLatitude,EnterLongitude,EnterTime,EnterMileage,Kind)");
                                                    stringBuilder3.Append(" values (");
                                                    stringBuilder3.Append("@ImeiNo,@SquadDate,@RouteID,@SiteID,@POIID,@EnterLatitude,@EnterLongitude,@EnterTime,@EnterMileage,@Kind)");
                                                    SqlParameter[] sqlParameterArray = new SqlParameter[10]
                                                    {
                            new SqlParameter("@ImeiNo", SqlDbType.VarChar, 50),
                            new SqlParameter("@SquadDate", SqlDbType.DateTime),
                            new SqlParameter("@RouteID", SqlDbType.Int, 4),
                            new SqlParameter("@SiteID", SqlDbType.Int, 4),
                            new SqlParameter("@POIID", SqlDbType.Int, 4),
                            new SqlParameter("@EnterLatitude", SqlDbType.VarChar, 50),
                            new SqlParameter("@EnterLongitude", SqlDbType.VarChar, 50),
                            new SqlParameter("@EnterTime", SqlDbType.DateTime),
                            new SqlParameter("@EnterMileage", SqlDbType.Float),
                            new SqlParameter("@Kind", SqlDbType.VarChar, 50)
                                                    };
                                                    sqlParameterArray[0].Value = (object)trackListPoiInfo.imeino;
                                                    sqlParameterArray[1].Value = (object)trackListPoiInfo.squaddate;
                                                    sqlParameterArray[2].Value = (object)trackListPoiInfo.routeid;
                                                    sqlParameterArray[3].Value = (object)trackListPoiInfo.siteid;
                                                    sqlParameterArray[4].Value = (object)trackListPoiInfo.poiid;
                                                    sqlParameterArray[5].Value = (object)trackListPoiInfo.enterlatitude;
                                                    sqlParameterArray[6].Value = (object)trackListPoiInfo.enterlongitude;
                                                    sqlParameterArray[7].Value = (object)trackListPoiInfo.entertime;
                                                    sqlParameterArray[8].Value = (object)trackListPoiInfo.entermileage;
                                                    sqlParameterArray[9].Value = (object)trackListPoiInfo.kind;
                                                    FileOpetation.SaveRecord(stringBuilder3.ToString() + "\r\n");
                                                    SqlHelper.ExecuteNonQuery(stringBuilder3.ToString(), sqlParameterArray);
                                                    this.poiht.Add((object)(row1["ImeiNO"].ToString() + "-" + (object)poiInfo.id), (object)trackListPoiInfo);
                                                    POIDefineInfo poiDefineInfo = (POIDefineInfo)this.carpoiht[(object)(carid.ToString() + "-" + (object)poiInfo.id)];
                                                    if (num32 == 0 && poiDefineInfo != null)
                                                    {
                                                        DateTime dateTime3 = DateTime.Parse(row1["GPSTime"].ToString());
                                                        string str2 = dateTime3.ToString("yyyy-MM-dd");
                                                        string str5 = dateTime3.ToString("HH:mm:ss");
                                                        if ((poiDefineInfo.always == 1 || (poiDefineInfo.weekdates.IndexOf(Utils.GetWeekDay(DateTime.Parse(row1["GPSTime"].ToString())).ToString()) >= 0 || poiDefineInfo.poidate.Equals(str2))) && str5.CompareTo(poiDefineInfo.starttime) >= 0 && str5.CompareTo(poiDefineInfo.endtime) <= 0)
                                                        {
                                                            num32 = poiInfo.id;
                                                            str9 = str9 + poiInfo.name + ",";
                                                            if (str3.IndexOf("POI Alarm") < 0)
                                                                str3 += "POI Alarm; ";
                                                        }
                                                    }
                                                }
                                            }
                                            else if (num15 > this.mindistance)
                                            {
                                                TrackListPOIInfo trackListPoiInfo = (TrackListPOIInfo)this.poiht[(object)(row1["ImeiNO"].ToString() + "-" + (object)poiInfo.id)];
                                                trackListPoiInfo.exitlatitude = row1["Latitude"].ToString();
                                                trackListPoiInfo.exitlongitude = row1["Longitude"].ToString();
                                                trackListPoiInfo.exitmileage = (double)row1["Mileage"];
                                                trackListPoiInfo.exittime = dateTime1;
                                                StringBuilder stringBuilder3 = new StringBuilder();
                                                stringBuilder3.Append("update tracklistpoi set ExitLatitude=@ExitLatitude,ExitLongitude=@ExitLongitude,ExitTime=@ExitTime,ExitMileage=@ExitMileage,State=1,Duration=@Duration,Mileage=@Mileage where ImeiNo=@ImeiNo and POIID=@POIID and State=0");
                                                SqlParameter[] sqlParameterArray = new SqlParameter[8]
                                                {
                          new SqlParameter("@ImeiNo", SqlDbType.VarChar, 50),
                          new SqlParameter("@POIID", SqlDbType.Int, 4),
                          new SqlParameter("@ExitLatitude", SqlDbType.VarChar, 50),
                          new SqlParameter("@ExitLongitude", SqlDbType.VarChar, 50),
                          new SqlParameter("@ExitTime", SqlDbType.DateTime),
                          new SqlParameter("@ExitMileage", SqlDbType.Float),
                          new SqlParameter("@Duration", SqlDbType.VarChar, 50),
                          new SqlParameter("@Mileage", SqlDbType.Float)
                                                };
                                                sqlParameterArray[0].Value = (object)trackListPoiInfo.imeino;
                                                sqlParameterArray[1].Value = (object)trackListPoiInfo.poiid;
                                                sqlParameterArray[2].Value = (object)trackListPoiInfo.exitlatitude;
                                                sqlParameterArray[3].Value = (object)trackListPoiInfo.exitlongitude;
                                                sqlParameterArray[4].Value = (object)trackListPoiInfo.exittime;
                                                sqlParameterArray[5].Value = (object)trackListPoiInfo.exitmileage;
                                                SqlParameter sqlParameter = sqlParameterArray[6];
                                                TimeSpan timeSpan = trackListPoiInfo.exittime.Subtract(trackListPoiInfo.entertime);
                                                timeSpan = timeSpan.Duration();
                                                string str2 = timeSpan.ToString("c").Substring(0, 8);
                                                sqlParameter.Value = (object)str2;
                                                sqlParameterArray[7].Value = (object)(trackListPoiInfo.exitmileage - trackListPoiInfo.entermileage);
                                                SqlHelper.ExecuteNonQuery(stringBuilder3.ToString(), sqlParameterArray);
                                                this.poiht.Remove((object)(row1["ImeiNO"].ToString() + "-" + (object)poiInfo.id));
                                            }
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    exception = ex;
                                }
                                if (Utils.tracklistht.ContainsKey((object)row1["imeino"].ToString()))
                                {
                                    TrackListInfo trackListInfo = (TrackListInfo)Utils.tracklistht[(object)row1["imeino"].ToString()];
                                    trackListInfo.carid = carInfo.carid;
                                    trackListInfo.latitude = row1["latitude"].ToString();
                                    trackListInfo.longitude = row1["longitude"].ToString();
                                    trackListInfo.gpstime = DateTime.Parse(row1["gpstime"].ToString());
                                    trackListInfo.engine = num21;
                                }
                                else
                                    Utils.tracklistht.Add((object)row1["imeino"].ToString(), (object)new TrackListInfo()
                                    {
                                        carid = carInfo.carid,
                                        latitude = row1["latitude"].ToString(),
                                        longitude = row1["longitude"].ToString(),
                                        gpstime = DateTime.Parse(row1["gpstime"].ToString()),
                                        engine = num21
                                    });
                                DataRow row2 = dt.NewRow();
                                row2["ImeiNo"] = (object)row1["imeino"].ToString();
                                row2["Latitude"] = (object)row1["latitude"].ToString();
                                row2["Longitude"] = (object)row1["longitude"].ToString();
                                row2["Speed"] = (object)num30;
                                row2["Direct"] = (object)str1;
                                row2["Fuel"] = (object)num23;
                                row2["Fuel2"] = (object)num24;
                                row2["Mileage"] = (object)num31;
                                row2["Temperature"] = row1["temperature"];
                                row2["GPSTime"] = (object)dateTime1;
                                row2["CreateTime"] = (object)DateTime.Parse(row1["createtime"].ToString());
                                row2["SquadDate"] = (object)dateTime2;
                                row2["Address"] = (object)xml;
                                row2["Input1"] = (object)num8;
                                row2["Input2"] = (object)num9;
                                row2["Input3"] = (object)num10;
                                row2["Input4"] = (object)num11;
                                row2["Input5"] = (object)num12;
                                row2["Output1"] = (object)num3;
                                row2["Output2"] = (object)num4;
                                row2["Output3"] = (object)num5;
                                row2["Output4"] = (object)num6;
                                row2["Output5"] = (object)num7;
                                row2["Engine"] = (object)num21;
                                row2["Working"] = (object)num22;
                                row2["AlarmText"] = (object)str3;
                                row2["StatusText"] = (object)str6;
                                row2["DriverID"] = (object)num25;
                                row2["DistrictID"] = (object)num27;
                                row2["BlockID"] = (object)num28;
                                row2["StackID"] = (object)num29;
                                row2["InGeofenceID"] = (object)num13;
                                row2["OutGeofenceID"] = (object)num14;
                                row2["TerminalID"] = (object)num26;
                                row2["StackAddress"] = (object)stringBuilder4;
                                row2["GPS"] = (object)str7;
                                row2["Signal"] = (object)str8;
                                row2["POIID"] = (object)num32;
                                dt.Rows.Add(row2);
                                if (!str3.Equals(""))
                                {
                                    string str2 = "insert into AlarmList(ImeiNo,Latitude,Longitude,Speed,Direct,Fuel,Fuel2,Fuel3,Fuel4,Mileage,Temperature,Engine,Input1,Input2,Input3,Input4,Input5,Output1,Output2,Output3,Output4,Output5,GPSTime,CreateTime,Address,SquadDate,AlarmText,StatusText,DriverID,TerminalID,DistrictID,BlockID,StackID,InGeofenceID,OutGeofenceID,GPS,Signal,POIID) values('" + row1["imeino"].ToString() + "','" + row1["latitude"].ToString() + "','" + row1["longitude"].ToString() + "'," + (object)num30 + ",'" + str1 + "'," + (object)num23 + "," + (object)num24 + "," + (object)0 + "," + (object)0 + "," + (object)num31 + "," + row1["temperature"] + "," + (object)num21 + "," + (object)num8 + "," + (object)num9 + "," + (object)num10 + "," + (object)num11 + "," + (object)num12 + "," + (object)num3 + "," + (object)num4 + "," + (object)num5 + "," + (object)num6 + "," + (object)num7 + ",'" + dateTime1.ToString("yyyy-MM-dd HH:mm:ss") + "','" + row1["createtime"].ToString() + "','" + xml.Replace("'", "''") + "','" + dateTime2.ToString("yyyy-MM-dd") + "','" + str3 + "','" + str6 + "'," + (object)num25 + "," + (object)num26 + "," + (object)num27 + "," + (object)num28 + "," + (object)num29 + "," + (object)num13 + "," + (object)num14 + ",'" + str7 + "','" + str8 + "'," + (object)num32 + ")";
                                    try
                                    {
                                        SqlHelper.ExecuteNonQuery(str2);
                                    }
                                    catch (Exception ex)
                                    {
                                        exception = ex;
                                        FileOpetation.SaveRecord(str2);
                                    }
                                }
                                string[] strArray1 = str3.Trim().TrimEnd(';').Split(';');
                                ArrayList arrayList1 = new ArrayList();
                                flag1 = false;
                                try
                                {
                                    for (int index1 = 0; index1 < strArray1.Length; ++index1)
                                    {
                                        if (!strArray1[index1].Trim().Equals(""))
                                        {
                                            using (SqlDataReader sqlDataReader = SqlHelper.ExecuteReader("select * from vEmail where carid=" + (object)carid + " and (AlarmNames='All' or AlarmNames like '%" + strArray1[index1] + "%')"))
                                            {
                                                while (sqlDataReader.Read())
                                                {
                                                    string[] strArray2 = sqlDataReader["Email"].ToString().Split(',');
                                                    for (int index2 = 0; index2 < strArray2.Length; ++index2)
                                                    {
                                                        if (!strArray2[index2].Trim().Equals(""))
                                                        {
                                                            if (xml.Trim().Equals(""))
                                                                xml = Utils.ReserveGeocode(s1 + "," + s2);
                                                            if (!arrayList1.Contains((object)strArray2[index2].Trim()))
                                                            {
                                                                arrayList1.Add((object)strArray2[index2].Trim());
                                                                CompanyInfo companyInfo = (CompanyInfo)this.companyht[(object)companyid.ToString()];
                                                                if (!companyInfo.emailhost.Equals(""))
                                                                {
                                                                    string title = carInfo.plate + " " + str3;
                                                                    string content = "Vehicle number: " + carInfo.plate + "\nSpeed:" + (object)num30 + "Km/h\nAlert Time: " + dateTime1.ToString("yyyy-MM-dd HH:mm:ss") + "\nAlert Event: " + str3 + (str4.Equals("") ? (object)"" : (object)("[" + str4 + "]")) + (str9.Equals("") ? (object)"" : (object)("[" + str9 + "]")) + "\nAddress: " + xml + "\nhttps://maps.google.com/maps?q=" + row1["latitude"].ToString() + "," + row1["longitude"].ToString();
                                                                    Utils.SendEmail(companyInfo.emailhost, companyInfo.emailport, companyInfo.emailusername, companyInfo.emailpassword, companyInfo.emailaddress, title, content, strArray2[index2].Trim());
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    exception = ex;
                                }
                                try
                                {
                                    ArrayList arrayList2 = new ArrayList();
                                    for (int index1 = 0; index1 < strArray1.Length; ++index1)
                                    {
                                        if (!strArray1[index1].Trim().Equals(""))
                                        {
                                            using (SqlDataReader sqlDataReader = SqlHelper.ExecuteReader("select * from vSms where carid=" + (object)carid + " and (AlarmNames='All' or AlarmNames like '%" + strArray1[index1] + "%')"))
                                            {
                                                while (sqlDataReader.Read())
                                                {
                                                    string[] strArray2 = sqlDataReader["Phone"].ToString().Split(',');
                                                    for (int index2 = 0; index2 < strArray2.Length; ++index2)
                                                    {
                                                        if (!strArray2[index2].Trim().Equals("") && !arrayList2.Contains((object)strArray2[index2].Trim()))
                                                        {
                                                            FileOpetation.SaveRecord("Send Sms To:" + strArray2[index2].Trim() + "\r\n");
                                                            arrayList2.Add((object)strArray2[index2].Trim());
                                                            CompanyInfo companyInfo = (CompanyInfo)this.companyht[(object)companyid.ToString()];
                                                            string str2 = "Vehicle:" + carInfo.plate + "\nAlert:" + str3;
                                                            if (str3.IndexOf("Speed") >= 0)
                                                                str2 = str2 + " " + (object)num30 + "km/h";
                                                            FileOpetation.SaveRecord(Utils.SendSms(str2 + "\n" + dateTime1.ToString("yyyy-MM-dd HH:mm:ss") + "\n https://maps.google.com/maps?q=" + row1["latitude"].ToString() + "," + row1["longitude"].ToString(), strArray2[index2].Trim()) + "\r\n");
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    FileOpetation.SaveRecord("Send SMS Error:" + ex.Message);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            FileOpetation.SaveRecord(string.Format("Processing with ID ={1},{0}Record abnormality:" + ex.Message + "\r\n" + ex.StackTrace.Substring(ex.StackTrace.IndexOf("line") + 4).Trim(), (object)DateTime.Now, (object)row1["imeino"].ToString()));
                        }
                    }
                }
                FileOpetation.SaveRecord("Logic Finish.\r\n");
                SqlHelper.ExecuteNonQuery("delete from track where id<=" + obj.ToString());
                FileOpetation.SaveRecord("Delete Finish.\r\n");
                Utils.SqlBulkCopyByDatatable(SqlHelper.CONN_STRING, "TrackList_Tmp1", dt);
                FileOpetation.SaveRecord("Save TrackList_Tmp Finish.\r\n");
                Utils.SqlBulkCopyByDatatable(SqlHelper.CONN_STRING, "TrackList", dt);
                FileOpetation.SaveRecord("Save TrackList Finish.\r\n");
                if (this.num >= 30)
                    this.num = 0;
                if (this.num % 20 == 0)
                    this.InitHashtable();
                FileOpetation.SaveRecord(string.Format("Current recording time：{0}, Number of pieces：{2},time cost：{1} Seconds, status: the program is running normally -{3}！", (object)DateTime.Now, (object)(DateTime.Now - now).Seconds, (object)num1.ToString(), (object)this.num) + "\r\n");
            }
            catch (Exception ex)
            {
                FileOpetation.SaveRecord(string.Format("Processing ID ={0}Record abnormality:" + ex.Message + "\r\n" + ex.ToString(), (object)DateTime.Now));
            }
            finally
            {
                this.handleing = false;
            }
        }

        //Commented Temporary
        //private void CheckScheduleReport()
        //{
        //    try
        //    {
        //        using (DataTable table = SqlHelper.ExecuteDataset("select * from schedulereport").Tables[0])
        //        {
        //            foreach (DataRow row in (InternalDataCollectionBase)table.Rows)
        //            {
        //                string cmdText = "";
        //                ReportDataSource reportDataSource = new ReportDataSource();
        //                reportDataSource.set_Name("DS");
        //                string str1 = "";
        //                string str2 = row["email"].ToString();
        //                using (SqlDataReader sqlDataReader = SqlHelper.ExecuteReader("select * from report where id=" + row["reportid"].ToString()))
        //                {
        //                    if (sqlDataReader.Read())
        //                    {
        //                        this.reportViewer1.set_ProcessingMode((ProcessingMode)0);
        //                        this.reportViewer1.get_LocalReport().set_ReportPath("RDLC/" + sqlDataReader["RDLCFile"].ToString());
        //                        cmdText = sqlDataReader["SQL"].ToString();
        //                        str1 = sqlDataReader["Name"].ToString();
        //                    }
        //                }
        //                using (SqlDataReader sqlDataReader = SqlHelper.ExecuteReader("select * from ReportParam where reportid=" + row["reportid"].ToString()))
        //                {
        //                    while (sqlDataReader.Read())
        //                    {
        //                        DateTime dateTime = DateTime.Now.AddDays(-1.0);
        //                        cmdText = !sqlDataReader["ParamName"].ToString().Equals("CarID") ? (!sqlDataReader["ParamName"].ToString().Equals("BSquadDate") && !sqlDataReader["ParamName"].ToString().Equals("ESquadDate") && !sqlDataReader["ParamName"].ToString().Equals("SquadDate") ? cmdText.Replace("{" + sqlDataReader["ParamName"] + "}", "") : cmdText.Replace("{" + sqlDataReader["ParamName"] + "}", sqlDataReader["SQL"].ToString().Replace("{" + sqlDataReader["ParamName"] + "}", dateTime.ToString("yyyy-MM-dd")))) : cmdText.Replace("{" + sqlDataReader["ParamName"] + "}", sqlDataReader["SQL"].ToString().Replace("{" + sqlDataReader["ParamName"] + "}", row["carid"].ToString()));
        //                    }
        //                }
        //                if (!cmdText.Equals(""))
        //                {
        //                    FileOpetation.SaveRecord(string.Format("SQL:" + cmdText + "\r\n", (object)DateTime.Now));
        //                    using (SqlConnection connection = new SqlConnection(SqlHelper.CONN_STRING))
        //                    {
        //                        connection.Open();
        //                        SqlCommand selectCommand = new SqlCommand(cmdText, connection);
        //                        SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(selectCommand);
        //                        sqlDataAdapter.SelectCommand.CommandTimeout = 118;
        //                        DataSet dataSet = new DataSet();
        //                        sqlDataAdapter.Fill(dataSet);
        //                        selectCommand.Dispose();
        //                        if (dataSet.Tables.Count > 0)
        //                            reportDataSource.set_Value((object)dataSet.Tables[0]);
        //                    }
        //                }
        //              ((Collection<ReportDataSource>)this.reportViewer1.get_LocalReport().get_DataSources()).Clear();
        //                ((Collection<ReportDataSource>)this.reportViewer1.get_LocalReport().get_DataSources()).Add(reportDataSource);
        //                this.reportViewer1.RefreshReport();
        //                string str3;
        //                string str4;
        //                string str5;
        //                string[] strArray;
        //                Warning[] warningArray;
        //                byte[] buffer = ((Report)this.reportViewer1.get_LocalReport()).Render("Excel", (string)null, ref str3, ref str4, ref str5, ref strArray, ref warningArray);
        //                string str6 = "Output/" + str1 + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xls";
        //                FileStream fileStream = new FileStream(str6, FileMode.Create);
        //                fileStream.Write(buffer, 0, buffer.Length);
        //                fileStream.Close();
        //                string str7 = str2;
        //                char[] chArray = new char[1] { ',' };
        //                foreach (string email in str7.Split(chArray))
        //                    Utils.SendAttachmentEmail("smtp.sina.com", "25", "hitsystem@sina.com", "hitsystem123", "hitsystem@sina.com", "Schedule Report-" + str1, "", email, str6);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        FileOpetation.SaveRecord(string.Format("Initialiaze Exception:" + ex.Message + "(" + ex.StackTrace.Substring(ex.StackTrace.IndexOf("line") + 4).Trim() + ")\r\n", (object)DateTime.Now));
        //    }
        //}

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                using (DataTable table = SqlHelper.ExecuteDataset("select * from schedulereport").Tables[0])
                {
                    foreach (DataRow row in (InternalDataCollectionBase)table.Rows)
                    {
                        string cmdText = "";
                        ReportDataSource reportDataSource = new ReportDataSource();
                       // reportDataSource.set_Name("DS");
                        string str1 = "";
                        string str2 = row["email"].ToString();
                        using (SqlDataReader sqlDataReader = SqlHelper.ExecuteReader("select * from report where id=" + row["reportid"].ToString()))
                        {
                            if (sqlDataReader.Read())
                            {
                                //this.reportViewer1.set_ProcessingMode((ProcessingMode)0);
                                //this.reportViewer1.get_LocalReport().set_ReportPath("RDLC/" + sqlDataReader["RDLCFile"].ToString());
                                cmdText = sqlDataReader["SQL"].ToString();
                                str1 = sqlDataReader["Name"].ToString();
                            }
                        }
                        using (SqlDataReader sqlDataReader = SqlHelper.ExecuteReader("select * from ReportParam where reportid=" + row["reportid"].ToString()))
                        {
                            while (sqlDataReader.Read())
                            {
                                DateTime dateTime = DateTime.Now.AddDays(-1.0);
                                cmdText = !sqlDataReader["ParamName"].ToString().Equals("CarID") ? (!sqlDataReader["ParamName"].ToString().Equals("BSquadDate") && !sqlDataReader["ParamName"].ToString().Equals("ESquadDate") && !sqlDataReader["ParamName"].ToString().Equals("SquadDate") ? cmdText.Replace("{" + sqlDataReader["ParamName"] + "}", "") : cmdText.Replace("{" + sqlDataReader["ParamName"] + "}", sqlDataReader["SQL"].ToString().Replace("{" + sqlDataReader["ParamName"] + "}", dateTime.ToString("yyyy-MM-dd")))) : cmdText.Replace("{" + sqlDataReader["ParamName"] + "}", sqlDataReader["SQL"].ToString().Replace("{" + sqlDataReader["ParamName"] + "}", row["carid"].ToString()));
                            }
                        }
                        if (!cmdText.Equals(""))
                        {
                            FileOpetation.SaveRecord(string.Format("SQL:" + cmdText + "\r\n", (object)DateTime.Now));
                            using (SqlConnection connection = new SqlConnection(SqlHelper.CONN_STRING))
                            {
                                connection.Open();
                                SqlCommand selectCommand = new SqlCommand(cmdText, connection);
                                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(selectCommand);
                                sqlDataAdapter.SelectCommand.CommandTimeout = 118;
                                DataSet dataSet = new DataSet();
                                sqlDataAdapter.Fill(dataSet);
                                selectCommand.Dispose();
                                if (dataSet.Tables.Count > 0)
                                {
                                    //reportDataSource.set_Value((object)dataSet.Tables[0]);
                                }
                            }
                        }
                      //((Collection<ReportDataSource>)this.reportViewer1.get_LocalReport().get_DataSources()).Clear();
                      //  ((Collection<ReportDataSource>)this.reportViewer1.get_LocalReport().get_DataSources()).Add(reportDataSource);
                        this.reportViewer1.RefreshReport();
                        string str3;
                        string str4;
                        string str5;
                        string[] strArray;
                        Warning[] warningArray;
                        //byte[] buffer = ((Report)this.reportViewer1.get_LocalReport()).Render("Excel", (string)null, ref str3, ref str4, ref str5, ref strArray, ref warningArray);
                        string str6 = "Output/" + str1 + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xls";
                        FileStream fileStream = new FileStream(str6, FileMode.Create);
                        //fileStream.Write(buffer, 0, buffer.Length);
                        fileStream.Close();
                        string str7 = str2;
                        char[] chArray = new char[1] { ',' };
                        foreach (string email in str7.Split(chArray))
                            Utils.SendAttachmentEmail("smtp.sina.com", "25", "hitsystem@sina.com", "hitsystem123", "hitsystem@sina.com", "Schedule Report-" + str1, "", email, str6);
                    }
                }
            }
            catch (Exception ex)
            {
                FileOpetation.SaveRecord(string.Format("Initialization exception:" + ex.Message + "(" + ex.StackTrace.Substring(ex.StackTrace.IndexOf("line") + 4).Trim() + ")\r\n", (object)DateTime.Now));
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.reportViewer1.RefreshReport();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DateTime now = DateTime.Now;
            HSSFWorkbook hssfworkbook = new HSSFWorkbook();
            this.CreateExcelBySQL(hssfworkbook, "company", "select * from company");
            this.CreateExcelBySQL(hssfworkbook, "team", "select * from team");
            this.CreateExcelBySQL(hssfworkbook, "car", "select * from car");
            this.CreateExcelBySQL(hssfworkbook, "driver", "select * from driver");
            this.CreateExcelBySQL(hssfworkbook, "tb_action", "select * from tb_action");
            this.CreateExcelBySQL(hssfworkbook, "tb_menu", "select * from tb_menu");
            this.CreateExcelBySQL(hssfworkbook, "tb_role", "select * from tb_role");
            this.CreateExcelBySQL(hssfworkbook, "tb_roleaction", "select * from tb_roleaction");
            this.CreateExcelBySQL(hssfworkbook, "tb_rolemenu", "select * from tb_rolemenu");
            this.CreateExcelBySQL(hssfworkbook, "tb_user", "select * from tb_user");
            this.CreateExcelBySQL(hssfworkbook, "tb_useraction", "select * from tb_useraction");
            this.CreateExcelBySQL(hssfworkbook, "tb_userdata", "select * from tb_userdata");
            this.CreateExcelBySQL(hssfworkbook, "tb_usermenu", "select * from tb_usermenu");
            this.CreateExcelBySQL(hssfworkbook, "tb_userrole", "select * from tb_userrole");
            this.CreateExcelBySQL(hssfworkbook, "usercompany", "select * from usercompany");
            this.CreateExcelBySQL(hssfworkbook, "userteam", "select * from userteam");
            this.CreateExcelBySQL(hssfworkbook, "usercar", "select * from usercar");
            this.CreateExcelBySQL(hssfworkbook, "userrole", "select * from userrole");
            this.CreateExcelBySQL(hssfworkbook, "useruser", "select * from useruser");
            this.CreateExcelBySQL(hssfworkbook, "userextend", "select * from userextend");
            using (FileStream fileStream = new FileStream("Backup/backup_" + now.ToString("yyyyMMddHHmmss") + ".xls", FileMode.Create))
            {
                hssfworkbook.Write((Stream)fileStream);
                fileStream.Close();
                fileStream.Dispose();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.ClockCallbackTask((object)null);
        }

        private void TogetherTimer_Tick(object sender, EventArgs e)
        {
            FileOpetation.SaveRecord("Timer..." + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\r\n");
            this.OtherCallbackTask((object)null);
        }

        private void ScanTimer_Tick(object sender, EventArgs e)
        {
            DateTime dateTime = DateTime.Now;
            dateTime = dateTime.AddDays(-1.0);
        }

        private void STTimer_Tick(object sender, EventArgs e)
        {
            if (this.transfering)
                return;
            this.transfering = true;
            try
            {
                for (int index = 0; index < 10; ++index)
                {
                    try
                    {
                        FileOpetation.SaveRecord("Transfering:" + (object)index);
                        SqlHelper.ExecuteNonQuery("insert into tracklist" + (object)index + " select * from tracklist0" + (object)index);
                        SqlHelper.ExecuteNonQuery("truncate table tracklist0" + (object)index);
                    }
                    catch (Exception ex)
                    {
                        FileOpetation.SaveRecord(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                this.transfering = false;
            }
        }

        protected ISheet CreateExcelBySQL(
          HSSFWorkbook hssfworkbook,
          string sheetname,
          string sql)
        {
            ISheet sheet = hssfworkbook.CreateSheet(sheetname);
            using (SqlDataReader sqlDataReader = SqlHelper.ExecuteReader(sql))
            {
                if (sqlDataReader.Read())
                {
                    if (sqlDataReader.FieldCount > 0)
                    {
                        int rownum1 = 0;
                        int fieldCount = sqlDataReader.FieldCount;
                        IRow row1 = sheet.CreateRow(rownum1);
                        for (int index = 0; index < fieldCount; ++index)
                            row1.CreateCell(index).SetCellValue(sqlDataReader.GetName(index));
                        int rownum2 = rownum1 + 1;
                        IRow row2 = sheet.CreateRow(rownum2);
                        for (int column = 0; column < fieldCount; ++column)
                            row2.CreateCell(column).SetCellValue(sqlDataReader[column].ToString());
                        int rownum3 = rownum2 + 1;
                        while (sqlDataReader.Read())
                        {
                            IRow row3 = sheet.CreateRow(rownum3);
                            for (int column = 0; column < fieldCount; ++column)
                                row3.CreateCell(column).SetCellValue(sqlDataReader[column].ToString());
                            ++rownum3;
                        }
                    }
                }
            }
            return sheet;
        }

        private void GenTrackListFence(DateTime d)
        {
            using (DataTable table = SqlHelper.ExecuteDataset("select * from vCar with(nolock) where state=0 and imeino='" + this.textBox2.Text.Trim() + "'").Tables[0])
            {
                foreach (DataRow row in (InternalDataCollectionBase)table.Rows)
                {
                    try
                    {
                        int num1 = (int)row["ID"];
                        int num2 = (int)row["CompanyID"];
                        ArrayList arrayList = new ArrayList();
                        using (SqlDataReader sqlDataReader = SqlHelper.ExecuteReader("select * from vCarFence where carid=" + (object)num1 + " and (Always=1 or FenceDate='" + d.ToString("yyyy-MM-dd") + "' or charindex('" + (object)Utils.GetWeekDay(d) + "',WeekDates)>0)"))
                        {
                            while (sqlDataReader.Read())
                                arrayList.Add((object)new FenceInfo()
                                {
                                    carfenceid = (int)sqlDataReader["ID"],
                                    geofenceid = (int)sqlDataReader["GeoFenceID"],
                                    name = sqlDataReader["Name"].ToString(),
                                    geofencekindid = (int)sqlDataReader["GeoFenceKindID"],
                                    radius = (double)sqlDataReader["Radius"],
                                    latlngs = sqlDataReader["Latlngs"].ToString(),
                                    inorout = (int)sqlDataReader["InOrOut"],
                                    starttime = sqlDataReader["StartTime"].ToString(),
                                    endtime = sqlDataReader["EndTime"].ToString()
                                });
                        }
                        FenceInfo fenceInfo1 = (FenceInfo)null;
                        TrackListFenceInfo trackListFenceInfo = (TrackListFenceInfo)null;
                        using (SqlDataReader sqlDataReader = SqlHelper.ExecuteReader("select * from tracklist with(nolock) where squaddate='" + d.ToString("yyyy-MM-dd") + "' and imeino='" + row["ImeiNo"] + "' order by GPSTime"))
                        {
                            while (sqlDataReader.Read())
                            {
                                if (fenceInfo1 == null)
                                {
                                    for (int index = 0; index < arrayList.Count; ++index)
                                    {
                                        bool flag = false;
                                        FenceInfo fenceInfo2 = (FenceInfo)arrayList[index];
                                        if (fenceInfo2.geofencekindid == 1)
                                        {
                                            string[] strArray = fenceInfo2.latlngs.Split('|');
                                            double num3 = Math.Min(double.Parse(strArray[0].Split(',')[0]), double.Parse(strArray[1].Split(',')[0]));
                                            double num4 = Math.Max(double.Parse(strArray[0].Split(',')[0]), double.Parse(strArray[1].Split(',')[0]));
                                            double num5 = Math.Min(double.Parse(strArray[0].Split(',')[1]), double.Parse(strArray[1].Split(',')[1]));
                                            double num6 = Math.Max(double.Parse(strArray[0].Split(',')[1]), double.Parse(strArray[1].Split(',')[1]));
                                            if (fenceInfo2.inorout == 0 && double.Parse(sqlDataReader["latitude"].ToString()) < num4 && (double.Parse(sqlDataReader["latitude"].ToString()) > num3 && double.Parse(sqlDataReader["longitude"].ToString()) < num6) && double.Parse(sqlDataReader["longitude"].ToString()) > num5 || fenceInfo2.inorout == 1 && (double.Parse(sqlDataReader["latitude"].ToString()) > num4 || double.Parse(sqlDataReader["latitude"].ToString()) < num3 || double.Parse(sqlDataReader["longitude"].ToString()) > num6 || double.Parse(sqlDataReader["longitude"].ToString()) < num5))
                                                flag = true;
                                        }
                                        else if (fenceInfo2.geofencekindid == 2)
                                        {
                                            string[] strArray = fenceInfo2.latlngs.Split(',');
                                            double distance = Utils.GetDistance(double.Parse(strArray[0]), double.Parse(strArray[1]), double.Parse(sqlDataReader["Latitude"].ToString()), double.Parse(sqlDataReader["Longitude"].ToString()));
                                            if (fenceInfo2.inorout == 0 && distance < fenceInfo2.radius || fenceInfo2.inorout == 1 && distance > fenceInfo2.radius)
                                                flag = true;
                                        }
                                        else if (fenceInfo2.geofencekindid == 3)
                                        {
                                            string[] ps = fenceInfo2.latlngs.Split('|');
                                            FileOpetation.SaveRecord(fenceInfo2.inorout.ToString() + "---------------" + (object)Utils.isPointInPolygon(ps, double.Parse(sqlDataReader["longitude"].ToString()), double.Parse(sqlDataReader["latitude"].ToString())) + "\r\n");
                                            if (fenceInfo2.inorout == 0 && Utils.isPointInPolygon(ps, double.Parse(sqlDataReader["longitude"].ToString()), double.Parse(sqlDataReader["latitude"].ToString())) || fenceInfo2.inorout == 1 && !Utils.isPointInPolygon(ps, double.Parse(sqlDataReader["longitude"].ToString()), double.Parse(sqlDataReader["latitude"].ToString())))
                                                flag = true;
                                        }
                                        if (flag)
                                        {
                                            FileOpetation.SaveRecord("SQL:Trigger.\r\n");
                                            fenceInfo1 = fenceInfo2;
                                            trackListFenceInfo = new TrackListFenceInfo();
                                            trackListFenceInfo.imeino = row["ImeiNO"].ToString();
                                            trackListFenceInfo.squaddate = DateTime.Parse(d.ToString("yyyy-MM-dd"));
                                            trackListFenceInfo.carfenceid = fenceInfo2.carfenceid;
                                            trackListFenceInfo.geofenceid = fenceInfo2.geofenceid;
                                            trackListFenceInfo.startlatitude = sqlDataReader["Latitude"].ToString();
                                            trackListFenceInfo.startlongitude = sqlDataReader["Longitude"].ToString();
                                            trackListFenceInfo.startmileage = double.Parse(sqlDataReader["Mileage"].ToString());
                                            trackListFenceInfo.starttime = DateTime.Parse(sqlDataReader["GPSTime"].ToString());
                                            trackListFenceInfo.duration = "";
                                            trackListFenceInfo.mileage = "";
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    bool flag = false;
                                    if (fenceInfo1.geofencekindid == 1)
                                    {
                                        string[] strArray = fenceInfo1.latlngs.Split('|');
                                        double num3 = Math.Min(double.Parse(strArray[0].Split(',')[0]), double.Parse(strArray[1].Split(',')[0]));
                                        double num4 = Math.Max(double.Parse(strArray[0].Split(',')[0]), double.Parse(strArray[1].Split(',')[0]));
                                        double num5 = Math.Min(double.Parse(strArray[0].Split(',')[1]), double.Parse(strArray[1].Split(',')[1]));
                                        double num6 = Math.Max(double.Parse(strArray[0].Split(',')[1]), double.Parse(strArray[1].Split(',')[1]));
                                        if (fenceInfo1.inorout == 1 && double.Parse(sqlDataReader["latitude"].ToString()) < num4 && (double.Parse(sqlDataReader["latitude"].ToString()) > num3 && double.Parse(sqlDataReader["longitude"].ToString()) < num6) && double.Parse(sqlDataReader["longitude"].ToString()) > num5 || fenceInfo1.inorout == 0 && (double.Parse(sqlDataReader["latitude"].ToString()) > num4 || double.Parse(sqlDataReader["latitude"].ToString()) < num3 || double.Parse(sqlDataReader["longitude"].ToString()) > num6 || double.Parse(sqlDataReader["longitude"].ToString()) < num5))
                                            flag = true;
                                    }
                                    else if (fenceInfo1.geofencekindid == 2)
                                    {
                                        string[] strArray = fenceInfo1.latlngs.Split(',');
                                        double distance = Utils.GetDistance(double.Parse(strArray[0]), double.Parse(strArray[1]), double.Parse(sqlDataReader["Latitude"].ToString()), double.Parse(sqlDataReader["Longitude"].ToString()));
                                        if (fenceInfo1.inorout == 1 && distance < fenceInfo1.radius || fenceInfo1.inorout == 0 && distance > fenceInfo1.radius)
                                            flag = true;
                                    }
                                    else if (fenceInfo1.geofencekindid == 3)
                                    {
                                        string[] ps = fenceInfo1.latlngs.Split('|');
                                        if (fenceInfo1.inorout == 1 && Utils.isPointInPolygon(ps, double.Parse(sqlDataReader["longitude"].ToString()), double.Parse(sqlDataReader["latitude"].ToString())) || fenceInfo1.inorout == 0 && !Utils.isPointInPolygon(ps, double.Parse(sqlDataReader["longitude"].ToString()), double.Parse(sqlDataReader["latitude"].ToString())))
                                            flag = true;
                                    }
                                    if (flag)
                                    {
                                        trackListFenceInfo.endlatitude = sqlDataReader["Latitude"].ToString();
                                        trackListFenceInfo.endlongitude = sqlDataReader["Longitude"].ToString();
                                        trackListFenceInfo.endmileage = double.Parse(sqlDataReader["Mileage"].ToString());
                                        trackListFenceInfo.endtime = DateTime.Parse(sqlDataReader["GPSTime"].ToString());
                                        TimeSpan timeSpan = trackListFenceInfo.endtime.Subtract(trackListFenceInfo.starttime);
                                        trackListFenceInfo.duration = Utils.FormatDuration(timeSpan.TotalSeconds);
                                        trackListFenceInfo.mileage = (trackListFenceInfo.endmileage - trackListFenceInfo.startmileage).ToString();
                                        StringBuilder stringBuilder = new StringBuilder();
                                        stringBuilder.Append("insert into tracklistfence(");
                                        stringBuilder.Append("ImeiNo,SquadDate,CarFenceID,GeoFenceID,StartLatitude,StartLongitude,StartTime,StartMileage,EndLatitude,EndLongitude,EndTime,EndMileage,Duration,Mileage)");
                                        stringBuilder.Append(" values (");
                                        stringBuilder.Append("@ImeiNo,@SquadDate,@CarFenceID,@GeoFenceID,@StartLatitude,@StartLongitude,@StartTime,@StartMileage,@EndLatitude,@EndLongitude,@EndTime,@EndMileage,@Duration,@Mileage)");
                                        SqlParameter[] sqlParameterArray = new SqlParameter[14]
                                        {
                      new SqlParameter("@ImeiNo", SqlDbType.VarChar, 50),
                      new SqlParameter("@SquadDate", SqlDbType.DateTime),
                      new SqlParameter("@CarFenceID", SqlDbType.Int, 4),
                      new SqlParameter("@GeoFenceID", SqlDbType.Int, 4),
                      new SqlParameter("@StartLatitude", SqlDbType.VarChar, 50),
                      new SqlParameter("@StartLongitude", SqlDbType.VarChar, 50),
                      new SqlParameter("@StartTime", SqlDbType.DateTime),
                      new SqlParameter("@StartMileage", SqlDbType.Float),
                      new SqlParameter("@EndLatitude", SqlDbType.VarChar, 50),
                      new SqlParameter("@EndLongitude", SqlDbType.VarChar, 50),
                      new SqlParameter("@EndTime", SqlDbType.DateTime),
                      new SqlParameter("@EndMileage", SqlDbType.Float),
                      new SqlParameter("@Duration", SqlDbType.VarChar, 50),
                      new SqlParameter("@Mileage", SqlDbType.VarChar, 50)
                                        };
                                        sqlParameterArray[0].Value = (object)trackListFenceInfo.imeino;
                                        sqlParameterArray[1].Value = (object)trackListFenceInfo.squaddate;
                                        sqlParameterArray[2].Value = (object)trackListFenceInfo.carfenceid;
                                        sqlParameterArray[3].Value = (object)trackListFenceInfo.geofenceid;
                                        sqlParameterArray[4].Value = (object)trackListFenceInfo.startlatitude;
                                        sqlParameterArray[5].Value = (object)trackListFenceInfo.startlongitude;
                                        sqlParameterArray[6].Value = (object)trackListFenceInfo.starttime;
                                        sqlParameterArray[7].Value = (object)trackListFenceInfo.startmileage;
                                        sqlParameterArray[8].Value = (object)trackListFenceInfo.endlatitude;
                                        sqlParameterArray[9].Value = (object)trackListFenceInfo.endlongitude;
                                        sqlParameterArray[10].Value = (object)trackListFenceInfo.endtime;
                                        sqlParameterArray[11].Value = (object)trackListFenceInfo.endmileage;
                                        sqlParameterArray[12].Value = (object)trackListFenceInfo.duration;
                                        sqlParameterArray[13].Value = (object)trackListFenceInfo.mileage;
                                        FileOpetation.SaveRecord("SQL:" + stringBuilder.ToString() + "\r\n");
                                        SqlHelper.ExecuteNonQuery(stringBuilder.ToString(), sqlParameterArray);
                                        fenceInfo1 = (FenceInfo)null;
                                    }
                                }
                            }
                        }
                        if (fenceInfo1 != null)
                        {
                            StringBuilder stringBuilder = new StringBuilder();
                            stringBuilder.Append("insert into tracklistfence(");
                            stringBuilder.Append("ImeiNo,SquadDate,CarFenceID,GeoFenceID,StartLatitude,StartLongitude,StartTime,StartMileage)");
                            stringBuilder.Append(" values (");
                            stringBuilder.Append("@ImeiNo,@SquadDate,@CarFenceID,@GeoFenceID,@StartLatitude,@StartLongitude,@StartTime,@StartMileage)");
                            SqlParameter[] sqlParameterArray = new SqlParameter[8]
                            {
                new SqlParameter("@ImeiNo", SqlDbType.VarChar, 50),
                new SqlParameter("@SquadDate", SqlDbType.DateTime),
                new SqlParameter("@CarFenceID", SqlDbType.Int, 4),
                new SqlParameter("@GeoFenceID", SqlDbType.Int, 4),
                new SqlParameter("@StartLatitude", SqlDbType.VarChar, 50),
                new SqlParameter("@StartLongitude", SqlDbType.VarChar, 50),
                new SqlParameter("@StartTime", SqlDbType.DateTime),
                new SqlParameter("@StartMileage", SqlDbType.Float)
                            };
                            sqlParameterArray[0].Value = (object)trackListFenceInfo.imeino;
                            sqlParameterArray[1].Value = (object)trackListFenceInfo.squaddate;
                            sqlParameterArray[2].Value = (object)trackListFenceInfo.carfenceid;
                            sqlParameterArray[3].Value = (object)trackListFenceInfo.geofenceid;
                            sqlParameterArray[4].Value = (object)trackListFenceInfo.startlatitude;
                            sqlParameterArray[5].Value = (object)trackListFenceInfo.startlongitude;
                            sqlParameterArray[6].Value = (object)trackListFenceInfo.starttime;
                            sqlParameterArray[7].Value = (object)trackListFenceInfo.startmileage;
                            FileOpetation.SaveRecord("SQL:" + stringBuilder.ToString() + "\r\n");
                            SqlHelper.ExecuteNonQuery(stringBuilder.ToString(), sqlParameterArray);
                        }
                    }
                    catch (Exception ex)
                    {
                        FileOpetation.SaveRecord("Scan Exception:" + ex.Message + "(" + ex.StackTrace.Substring(ex.StackTrace.IndexOf("line") + 4).Trim() + ")\r\n");
                    }
                }
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.GenTrackListFence(DateTime.Parse(this.textBox1.Text.Trim()));
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Utils.SendEmail("smtp.gmail.com", "587", "seecuritytracker@gmail.com", "@Noor2004-Seecurity", "seecuritytracker@gmail.com", "Test Email", "A Email to Test.", "gaellebs@outlook.com");
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing && this.components != null)
        //        this.components.Dispose();
        //    base.Dispose(disposing);
        //}

        private void MyInitializeComponent()
        {
            this.components = (IContainer)new Container();
            this.LabNum = new Label();
            this.button1 = new Button();
            this.reportViewer1 = new ReportViewer();
            this.ChkScan = new CheckBox();
            this.button2 = new Button();
            this.ClockTimer = new System.Windows.Forms.Timer(this.components);
            this.TogetherTimer = new System.Windows.Forms.Timer(this.components);
            this.ScanTimer = new System.Windows.Forms.Timer(this.components);
            this.STTimer = new System.Windows.Forms.Timer(this.components);
            this.button5 = new Button();
            this.textBox1 = new TextBox();
            this.button3 = new Button();
            this.textBox2 = new TextBox();
            this.SuspendLayout();
            this.LabNum.AutoSize = true;
            this.LabNum.Font = new Font("Microsoft YaHei", 60f, FontStyle.Regular, GraphicsUnit.Point, (byte)134);
            this.LabNum.Location = new Point(12, 57);
            this.LabNum.Name = "LabNum";
            this.LabNum.Size = new Size(92, 104);
            this.LabNum.TabIndex = 0;
            this.LabNum.Text = "0";
            this.button1.Location = new Point(653, 214);
            this.button1.Name = "button1";
            this.button1.Size = new Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new EventHandler(this.button1_Click);
            ((Control)this.reportViewer1).Location = new Point(197, 12);
            ((Control)this.reportViewer1).Name = "reportViewer1";
            ((Control)this.reportViewer1).Size = new Size(396, 246);
            ((Control)this.reportViewer1).TabIndex = 2;
            this.ChkScan.AutoSize = true;
            this.ChkScan.Location = new Point(653, 114);
            this.ChkScan.Name = "ChkScan";
            this.ChkScan.Size = new Size(48, 16);
            this.ChkScan.TabIndex = 3;
            this.ChkScan.Text = "Scan";
            this.ChkScan.UseVisualStyleBackColor = true;
            this.button2.Location = new Point(734, 214);
            this.button2.Name = "button2";
            this.button2.Size = new Size(75, 23);
            this.button2.TabIndex = 4;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new EventHandler(this.button2_Click);
            this.ClockTimer.Enabled = true;
            this.ClockTimer.Interval = 1000;
            this.ClockTimer.Tick += new EventHandler(this.timer1_Tick);
            this.TogetherTimer.Interval = 300000;
            this.TogetherTimer.Tick += new EventHandler(this.TogetherTimer_Tick);
            this.ScanTimer.Interval = 86400000;
            this.ScanTimer.Tick += new EventHandler(this.ScanTimer_Tick);
            this.STTimer.Interval = 180000;
            this.STTimer.Tick += new EventHandler(this.STTimer_Tick);
            this.button5.Location = new Point(626, 57);
            this.button5.Name = "button5";
            this.button5.Size = new Size(102, 23);
            this.button5.TabIndex = 10;
            this.button5.Text = "TrackListFence";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new EventHandler(this.button5_Click);
            this.textBox1.Location = new Point(628, 30);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Size(100, 21);
            this.textBox1.TabIndex = 9;
            this.button3.Location = new Point(729, 165);
            this.button3.Name = "button3";
            this.button3.Size = new Size(75, 23);
            this.button3.TabIndex = 11;
            this.button3.Text = "button3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new EventHandler(this.button3_Click);
            this.textBox2.Location = new Point(746, 30);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Size(100, 21);
            this.textBox2.TabIndex = 12;
            this.AutoScaleDimensions = new SizeF(6f, 12f);
            this.AutoScaleMode = AutoScaleMode.Font;
            this.ClientSize = new Size(858, 262);
            this.Controls.Add((Control)this.textBox2);
            this.Controls.Add((Control)this.button3);
            this.Controls.Add((Control)this.button5);
            this.Controls.Add((Control)this.textBox1);
            this.Controls.Add((Control)this.button2);
            this.Controls.Add((Control)this.ChkScan);
            this.Controls.Add((Control)this.reportViewer1);
            this.Controls.Add((Control)this.button1);
            this.Controls.Add((Control)this.LabNum);
            this.ForeColor = Color.Blue;
            this.Name = nameof(Form1);
            this.Text = "LogicApp";
            this.Load += new EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();
        }
    }
}
